<?php

/*
 * This file is part of FacturaScripts
 * Copyright (C) 2013-2017  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of ______________
 *
 * @author ________
 */
class articulo_um extends fs_model
{
    public $articuloref;
    public $claveum;
    public $nombreum;

    public function __construct($data = false)
    {
        parent::__construct('articulo_um');

        if ($data) {
            $this->load_from_data($data);
        } else {
            $this->clear();
        }
    }

    public function exists()
    {
        if (is_null($this->articuloref)) {
            return FALSE;
        }

        return $this->db->select("SELECT * FROM " . $this->table_name . " WHERE articuloref = " . $this->var2str($this->articuloref) . ";");
    }

    public function clear()
    {
        $this->articuloref = '';
        $this->claveum = 'H87';
        $this->nombreum = 'PIEZA';
    }

    public function load_from_data($data)
    {
        $this->articuloref = $data['articuloref'];
        $this->claveum = $data['claveum'];
        $this->nombreum = $data['nombreum'];
    }

    public function install()
    {
        return '';
    }

    public function get($ref)
    {
        $sql = 'SELECT * FROM ' . $this->table_name . ' WHERE articuloref = ' . $this->var2str($ref) . ';' ; 

        $data = $this->db->select($sql);
        if ($data) {
            return new articulo_um($data[0]);
        }
        return FALSE;
    }

    public function save()
    {
        if ($this->exists()) {
           $sql = 'UPDATE articulo_um SET '
            . '  claveum = ' . $this->var2str($this->claveum)
            . ', nombreum = ' . $this->var2str($this->nombreum)
            . ' WHERE articuloref = ' . $this->var2str($this->articuloref) . ';';
        } else {
            $sql = 'INSERT INTO articulo_um '
            . '(articuloref,claveum,nombreum) VALUES ' .
            '(' . $this->var2str($this->articuloref) .
            ',' . $this->var2str($this->claveum) .
            ',' . $this->var2str($this->nombreum) . ');';
        }
        return $this->db->exec($sql);
    }

    public function delete()
    {
        
    }
}
