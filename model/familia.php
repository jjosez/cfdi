<?php
/*
 * This file is part of facturacion_base
 * Copyright (C) 2013-2017  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'plugins/facturacion_base/model/core/familia.php';

/**
 * Una familia o categoría de artículos.
 * 
 * @author Carlos García Gómez <neorazorx@gmail.com>
 */
class familia extends FacturaScripts\model\familia
{
    public $clavesat;

    public function __construct($data = FALSE)
    {
        parent::__construct($data);

        if ($data) {
            $this->clavesat = $data['clavesat'];
        } else {
            $this->clavesat = '';
        }
    }

    /**
     * Guarda los datos en la base de datos
     * @return boolean
     */
    public function save()
    {
        if ($this->clavesat == '') {
            if ($this->madre) {
                $madre = $this->get($this->madre);
                if ($madre->clavesat) {
                    $this->clavesat = $madre->clavesat;
                } else {
                    $this->clavesat = '01010101';
                }
            }
        }

        if ($this->test()) {
            $this->cache->delete('m_familia_all');

            if ($this->exists()) {
                $sql = "UPDATE " . $this->table_name . " SET descripcion = " 
                    . $this->var2str($this->descripcion) 
                    . "," . " madre = " . $this->var2str($this->madre) 
                    . "," . " clavesat = " . $this->var2str($this->clavesat)  
                    . " WHERE codfamilia = " . $this->var2str($this->codfamilia) . ";";
            } else {
                $sql = "INSERT INTO " . $this->table_name . " (codfamilia,descripcion,madre,clavesat) VALUES " .
                    "(" . $this->var2str($this->codfamilia) .
                    "," . $this->var2str($this->descripcion) .
                    "," . $this->var2str($this->madre) . 
                    "," . $this->var2str($this->clavesat) . ");";
            }

            return $this->db->exec($sql);
        }

        return FALSE;
    }

    public function imagen_url()
    {
        $filename = str_replace('/', '__', $this->codfamilia);

        if (file_exists("tmp/" . FS_TMP_NAME . "images/familias/" . $filename . ".png")) {
            return FS_PATH . "tmp/" . FS_TMP_NAME . "images/familias/" . $filename . ".png";
        } else if (file_exists("tmp/" . FS_TMP_NAME . "images/familias/" . $filename . ".jpg")) {
            return FS_PATH . "tmp/" . FS_TMP_NAME . "images/familias/" . $filename . ".jpg";
        }

        return FS_PATH . 'plugins/tpv_tactil/view/img/folder.png';
    }

}
