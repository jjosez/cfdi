<?php
/*
 * This file is part of facturacion_base
 * Copyright (C) 2013-2017  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'plugins/facturacion_base/model/core/factura_cliente.php';

/**
 * Factura de un cliente.
 * 
 * @author Carlos García Gómez <neorazorx@gmail.com>
 */
class factura_cliente extends FacturaScripts\model\factura_cliente
{
    public $ptecfdi;

    public function __construct($data = FALSE)
    {
        parent::__construct($data);

        if ($data) {
            $this->ptecfdi = $this->str2bool($data['ptecfdi']);       
        } else {
            $this->ptecfdi = TRUE;
        }
    }

    public function install()
    {
        parent::install();
        return '';
    }

    public function save()
    {
        if (parent::save()) {
            $sql = "UPDATE " . $this->table_name . " SET ptecfdi = " . $this->var2str($this->ptecfdi)
            . "  WHERE idfactura = " . $this->var2str($this->idfactura) . ";";

            return $this->db->exec($sql);
        }

        return FALSE;
    }

    public function exists()
    {
        if (is_null($this->idfactura)) {
            $this->ptecfdi = TRUE;
            return FALSE;
        }

        return $this->db->select("SELECT * FROM " . $this->table_name . " WHERE idfactura = " . $this->var2str($this->idfactura) . ";");
    }

    public function all_ptecfdi()
    {
        $factlist = array();

        $sql = "SELECT * FROM " . $this->table_name . " WHERE ptecfdi = true;";
        $data = $this->db->select($sql);

        if ($data) {
            foreach ($data as $a) {
                $factlist[] = new \factura_cliente($a);
            }
        }

        return $factlist;
    }

    public function get_albaranes()
    {
    	$alist = array();

    	$sql = "SELECT idalbaran FROM lineasfacturascli WHERE idfactura = " . $this->var2str($this->idfactura) 
    	. "GROUP BY idalbaran;";

    	$data = $this->db->select($sql);

    	if ($data) {
    		foreach ($data as $d) {
                $alist[] = $d['idalbaran'];                
            }
            return $alist;
    	}

    	return false;
    }

    public function total_calculado()
    {
        $q="SELECT a.idfactura, a.cifnif, a.nombrecliente FROM facturascli AS a WHERE NOT EXISTS(SELECT * FROM cfdi AS b WHERE b.idfactura = a.idfactura)";
    }
}
