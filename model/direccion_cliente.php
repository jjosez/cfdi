<?php
/*
 * This file is part of facturacion_base
 * Copyright (C) 2014-2017  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'plugins/facturacion_base/model/core/direccion_cliente.php';

/**
 * Una dirección de un cliente. Puede tener varias.
 * 
 * @author Carlos García Gómez <neorazorx@gmail.com>
 */
class direccion_cliente extends FacturaScripts\model\direccion_cliente
{
    public $municipio;
    public $numero_ext;
    public $numero_int;
    public $colonia;

    public function __construct($data = FALSE)
    {
        parent::__construct($data);

        if ($data) {
            $this->municipio = $data['municipio'];
            $this->numero_ext = $data['numero_ext'];
            $this->numero_int = $data['numero_int'];
            $this->colonia = $data['colonia'];
        } else {
            $this->municipio = NULL;
            $this->numero_ext = NULL;
            $this->numero_int = NULL;
            $this->colonia = NULL;
        }
    }

    public function save()
    {
        //Datos extra para mexico
        $this->municipio = $this->no_html($this->municipio);
        $this->numero_ext = $this->no_html($this->numero_ext);
        $this->numero_int = $this->no_html($this->numero_int);
        $this->colonia = $this->no_html($this->colonia);

        if (parent::save()) {
            $sql = "UPDATE " . $this->table_name . " SET municipio = " . $this->var2str($this->municipio)
                . ", numero_ext = " . $this->var2str($this->numero_ext)
                . ", numero_int = " . $this->var2str($this->numero_int)
                . ", colonia = " . $this->var2str($this->colonia)
                . "  WHERE id = " . $this->var2str($this->id) . ";";

            return $this->db->exec($sql);
        }

        return FALSE;
    }

    public function save2()
    {
        $this->apartado = $this->no_html($this->apartado);
        $this->ciudad = $this->no_html($this->ciudad);
        $this->codpostal = $this->no_html($this->codpostal);
        $this->descripcion = $this->no_html($this->descripcion);
        $this->direccion = $this->no_html($this->direccion);
        $this->provincia = $this->no_html($this->provincia);

        //Datos extra para mexico
        $this->municipio = $this->no_html($this->municipio);
        $this->numero_ext = $this->no_html($this->numero_ext);
        $this->numero_int = $this->no_html($this->numero_int);
        $this->colonia =$this->no_html($this->colonia);

        /// actualizamos la fecha de modificación
        $this->fecha = date('d-m-Y');

        /// ¿Desmarcamos las demás direcciones principales?
        $sql = "";
        if ($this->domenvio) {
            $sql .= "UPDATE " . $this->table_name . " SET domenvio = false"
                . " WHERE codcliente = " . $this->var2str($this->codcliente) . ";";
        }
        if ($this->domfacturacion) {
            $sql .= "UPDATE " . $this->table_name . " SET domfacturacion = false"
                . " WHERE codcliente = " . $this->var2str($this->codcliente) . ";";
        }

        if ($this->exists()) {
            $sql .= "UPDATE " . $this->table_name . " SET codcliente = " . $this->var2str($this->codcliente)
                . ", codpais = " . $this->var2str($this->codpais)
                . ", apartado = " . $this->var2str($this->apartado)
                . ", provincia = " . $this->var2str($this->provincia)
                . ", ciudad = " . $this->var2str($this->ciudad)
                . ", codpostal = " . $this->var2str($this->codpostal)
                . ", direccion = " . $this->var2str($this->direccion)
                . ", domenvio = " . $this->var2str($this->domenvio)
                . ", domfacturacion = " . $this->var2str($this->domfacturacion)
                . ", descripcion = " . $this->var2str($this->descripcion)
                . ", fecha = " . $this->var2str($this->fecha)
                . ", municipio = " . $this->var2str($this->municipio)
                . ", numero_ext = " . $this->var2str($this->numero_ext)
                . ", numero_int = " . $this->var2str($this->numero_int)
                . ", colonia = " . $this->var2str($this->colonia)
                . "  WHERE id = " . $this->var2str($this->id) . ";";

            return $this->db->exec($sql);
        }

        $sql .= "INSERT INTO " . $this->table_name . " (codcliente,codpais,apartado,provincia,ciudad,codpostal,
            direccion,domenvio,domfacturacion,descripcion,fecha,municipio,numero_ext,numero_int,colonia) VALUES (" . $this->var2str($this->codcliente)
            . "," . $this->var2str($this->codpais)
            . "," . $this->var2str($this->apartado)
            . "," . $this->var2str($this->provincia)
            . "," . $this->var2str($this->ciudad)
            . "," . $this->var2str($this->codpostal)
            . "," . $this->var2str($this->direccion)
            . "," . $this->var2str($this->domenvio)
            . "," . $this->var2str($this->domfacturacion)
            . "," . $this->var2str($this->descripcion)
            . "," . $this->var2str($this->municipio)
            . "," . $this->var2str($this->numero_ext)
            . "," . $this->var2str($this->numero_int)
            . "," . $this->var2str($this->colonia) . ");";

        if ($this->db->exec($sql)) {
            $this->id = $this->db->lastval();
            return TRUE;
        }

        return FALSE;
    }

	public function direccion_facturacion($cod)
    {
        $dirlist = array();
        $sql = "SELECT * FROM " . $this->table_name . " WHERE codcliente = " . $this->var2str($cod)
            . " AND domfacturacion = 1 ORDER BY id DESC;";

        $data = $this->db->select($sql);

        if ($data) {
            return new \direccion_cliente($data[0]);
        }

        return FALSE;
    }  

    public function get_billing_address($cod)
    {
        $dirlist = array();
        $sql = "SELECT * FROM " . $this->table_name . " WHERE codcliente = " . $this->var2str($cod)
            . " AND domfacturacion = 1 ORDER BY id DESC;";

        $data = $this->db->select($sql);

        if ($data) {
            return new \direccion_cliente($data[0]);
        }

        return FALSE;
    }  
}
