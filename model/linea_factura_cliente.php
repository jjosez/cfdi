<?php
/*
 * This file is part of facturacion_base
 * Copyright (C) 2013-2017  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'plugins/facturacion_base/model/core/linea_factura_cliente.php';

/**
 * Línea de una factura de cliente.
 * 
 * @author Carlos García Gómez <neorazorx@gmail.com>
 */
class linea_factura_cliente extends FacturaScripts\model\linea_factura_cliente
{
	public function iva_pvp()
    {
        return $this->pvpunitario * ($this->iva) / 100;
    }

    public function iva_total()
    {
    	//$totalsindto = $this->pvpunitario * $this->cantidad;
        //$dto_due = (1-((1-$this->dtopor/100)*(1-$this->dtopor2/100)*(1-$this->dtopor3/100)*(1-$this->dtopor4/100)))*100;
        //$total = $totalsindto * (1 - $dto_due / 100);

        return $this->pvptotal * ($this->iva - $this->irpf + $this->recargo) / 100;
    }

    public function dto_total()
    {
    	$totalsindto = $this->pvpunitario * $this->cantidad;
        $dto_due = (1-((1-$this->dtopor/100)*(1-$this->dtopor2/100)*(1-$this->dtopor3/100)*(1-$this->dtopor4/100)))*100;
        return $totalsindto * ($dto_due / 100);

        //return $this->pvptotal * ($this->iva - $this->irpf + $this->recargo) / 100;
    }
    
}
