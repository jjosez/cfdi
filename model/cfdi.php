<?php

/**
 * Description of ______________
 *
 * @author ________
 */

class cfdi extends fs_model
{
    public $id;
    public $codcliente;
    public $idfactura;
    public $folio;
    public $version;
    public $uuid;
    public $cadenasat;
    public $tipocomp;
    public $condpago;
    public $metopago;
    public $formpago;
    public $fecha;
    public $fechamod;
    public $subtotal;
    public $descuento;
    public $iva;
    public $total;
    public $estatus;
    public $xml;
    public $xmlcancelado;
    public $rfcemisor;
    public $rfcreceptor;
    public $razonreceptor;
    public $cpexpedicion;
    public $moneda;
    public $tipocambio;
    public $usocod;
    public $regimenemisor;
    public $cfdirelacion;
    public $tiporelacion;
    public $cancelado;

    public function __construct($data = false)
    {
        parent::__construct('cfdi');

        if ($data) {
            $this->load_from_data($data);
        } else {
            $this->clear();
        }
    }

    public function clear()
    {
        $this->id = null;
        $this->codcliente = '';
        $this->idfactura = '';
        $this->folio = '';
        $this->version = '';
        $this->uuid = '';
        $this->cadenasat = '';
        $this->tipocomp = '';
        $this->condpago = '';
        $this->metopago = '';
        $this->formpago = '';
        $this->fecha = null;
        $this->fechamod = null;
        $this->subtotal = '';
        $this->descuento = '';
        $this->iva = '';
        $this->total = '';
        $this->estatus = '';
        $this->xml = '';
        $this->xmlcancelado = null;
        $this->rfcemisor = '';
        $this->rfcreceptor = '';
        $this->razonreceptor = '';
        $this->cpexpedicion = '';
        $this->moneda = '';
        $this->tipocambio = '';
        $this->usocod = '';
        $this->regimenemisor = '';
        $this->cfdirelacion = null;
        $this->tiporelacion = null;
        $this->cancelado = false;
    }

    public function load_from_data($data)
    {
        $this->id = $data['id'];
        $this->codcliente = $data['codcliente'];
        $this->idfactura = $data['idfactura'];
        $this->folio = $data['folio'];
        $this->version = $data['version'];
        $this->uuid = $data['uuid'];
        $this->cadenasat = $data['cadenasat'];
        $this->tipocomp = $data['tipocomp'];
        $this->condpago = $data['condpago'];
        $this->metopago = $data['metopago'];
        $this->formpago = $data['formpago'];
        $this->fecha = $data['fecha'];
        $this->fechamod = $data['fechamod'];
        $this->subtotal = $data['subtotal'];
        $this->descuento = $data['descuento'];
        $this->iva = $data['iva'];
        $this->total = $data['total'];
        $this->estatus = $data['estatus'];
        $this->xml = $data['xml'];
        $this->xmlcancelado = $data['xmlcancelado'];
        $this->rfcemisor = $data['rfcemisor'];
        $this->rfcreceptor = $data['rfcreceptor'];
        $this->razonreceptor = $data['razonreceptor'];
        $this->cpexpedicion = $data['cpexpedicion'];
        $this->moneda = $data['moneda'];
        $this->tipocambio = $data['tipocambio'];
        $this->usocod = $data['usocod'];
        $this->regimenemisor = $data['regimenemisor'];
        $this->cfdirelacion = $data['cfdirelacion'];
        $this->tiporelacion = $data['tiporelacion'];
        $this->cancelado = $this->str2bool($data['cancelado']);
    }

    public function install()
    {
        return '';
    }

    public function exists()
    {
        if (is_null($this->id)) {
            return false;
        }

        return true;
    }

    public function save()
    {
        if ($this->exists()) {
            $sql = "UPDATE " . $this->table_name . " SET codcliente = " . $this->var2str($this->codcliente)
            . ", idfactura = " . $this->var2str($this->idfactura)
            . ", folio" . $this->var2str($this->folio)
            . ", version" . $this->var2str($this->version)
            . ", uuid" . $this->var2str($this->uuid)
            . ", cadenasat" . $this->var2str($this->cadenasat)
            . ", tipocomp" . $this->var2str($this->tipocomp)
            . ", condpago" . $this->var2str($this->condpago)
            . ", metopago" . $this->var2str($this->metopago)
            . ", formpago" . $this->var2str($this->formpago)
            . ", fecha" . $this->var2str($this->fecha)
            . ", fechamod" . $this->var2str($this->fechamod)
            . ", subtotal" . $this->var2str($this->subtotal)
            . ", descuento" . $this->var2str($this->descuento)
            . ", iva" . $this->var2str($this->iva)
            . ", total" . $this->var2str($this->total)
            . ", estatus" . $this->var2str($this->estatus)
            . ", xml" . $this->var2str($this->xml)
            . ", xmlcancelado" . $this->var2str($this->xmlcancelado)
            . ", rfcemisor" . $this->var2str($this->rfcemisor)
            . ", rfcreceptor" . $this->var2str($this->rfcreceptor)
            . ", razonreceptor" . $this->var2str($this->razonreceptor)
            . ", cpexpedicion" . $this->var2str($this->cpexpedicion)
            . ", moneda" . $this->var2str($this->moneda)
            . ", tipocambio" . $this->var2str($this->tipocambio)
            . ", usocod" . $this->var2str($this->usocod)
            . ", regimenemisor" . $this->var2str($this->regimenemisor)
            . ", cfdirelacion" . $this->var2str($this->cfdirelacion)
            . ", tiporelacion" . $this->var2str($this->tiporelacion)
            . ", cancelado" . $this->var2str($this->cancelado) 
            . "  WHERE id = " . $this->var2str($this->id) . ";";
        } else {
            $sql = "INSERT INTO " . $this->table_name . " (codcliente,idfactura,folio,version,"
            . "uuid,cadenasat,tipocomp,condpago,metopago,formpago,fecha,fechamod,subtotal,descuento,iva,total,"
            . "estatus,xml,xmlcancelado,rfcemisor,rfcreceptor,razonreceptor,cpexpedicion,moneda,tipocambio,usocod,"
            . "regimenemisor,cfdirelacion,tiporelacion,cancelado ) VALUES ("
            . $this->var2str($this->codcliente)
            . "," . $this->var2str($this->idfactura)
            . "," . $this->var2str($this->folio)
            . "," . $this->var2str($this->version)
            . "," . $this->var2str($this->uuid)
            . "," . $this->var2str($this->cadenasat)
            . "," . $this->var2str($this->tipocomp)
            . "," . $this->var2str($this->condpago)
            . "," . $this->var2str($this->metopago)
            . "," . $this->var2str($this->formpago)
            . "," . $this->var2str($this->fecha)
            . "," . $this->var2str($this->fechamod)
            . "," . $this->var2str($this->subtotal)
            . "," . $this->var2str($this->descuento)
            . "," . $this->var2str($this->iva)
            . "," . $this->var2str($this->total)
            . "," . $this->var2str($this->estatus)
            . "," . $this->var2str($this->xml)
            . "," . $this->var2str($this->xmlcancelado)
            . "," . $this->var2str($this->rfcemisor)
            . "," . $this->var2str($this->rfcreceptor)
            . "," . $this->var2str($this->razonreceptor)
            . "," . $this->var2str($this->cpexpedicion)
            . "," . $this->var2str($this->moneda)
            . "," . $this->var2str($this->tipocambio)
            . "," . $this->var2str($this->usocod)
            . "," . $this->var2str($this->regimenemisor)
            . "," . $this->var2str($this->cfdirelacion)
            . "," . $this->var2str($this->tiporelacion)
            . "," . $this->var2str($this->cancelado) . ");";
        }

        if ($this->db->exec($sql)) {
            $this->id = $this->db->lastval();
            return true;
        }

        return false;
    }

    public function delete()
    {
    }

    public function url()
    {
        if ($this->id) {
            return 'index.php?page=cfdi_detalle&cfdi=' . $this->id;
        }

        return 'index.php?page=comprobantes_cfdi';
    }

    public function cliente_url()
    {
        if ($this->id) {
            return 'index.php?page=ventas_cliente&cod=' . $this->codcliente;
        }

        return false;
    }

    public function nombrecliente()
    {
        $clientes = new \cliente();
        $cliente = $clientes->get($this->codcliente);
        
        if ($cliente->razonsocial) {
            return $cliente->razonsocial;
        }

        return false;
    }

    public function all($offset = 0, $limit = FS_ITEM_LIMIT)
    {
        $cflist = array();
        $sql = "SELECT * FROM " . $this->table_name . " ORDER BY folio DESC";

        $data = $this->db->select_limit($sql, $limit, $offset);

        if ($data) {
            foreach ($data as $d) {
                $cflist[] = new cfdi($d);
            }

            return $cflist;
        }

        return false;
    }

    public function fs_sql_total($tabla, $columna, $where = '')
    {
        $data = $this->db->select("SELECT COUNT(" . $columna . ") as total FROM " . $tabla . ' ' . $where . ";");

        if ($data) {
            return intval($data[0]['total']);
        }

        return 0;
    }

    public function get($id)
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE id = "
        . $this->var2str($id) . ";";

        $data = $this->db->select($sql);

        if ($data) {
            return new \cfdi($data[0]);
        }

        return false;
    }

    public function get_from_factura($id)
    {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE idfactura = "
        . $this->var2str($id) . ";";

        $data = $this->db->select($sql);

        if ($data) {
            return new \cfdi($data[0]);
        }

        return false;
    }
}
