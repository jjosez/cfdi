<?php
/*
 * This file is part of facturacion_base
 * Copyright (C) 2013-2017  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'plugins/facturacion_base/model/core/albaran_cliente.php';

/**
 * Albarán de cliente o albarán de venta. Representa la entrega a un cliente
 * de un material que se le ha vendido. Implica la salida de ese material
 * del almacén de la empresa.
 * 
 * @author Carlos García Gómez <neorazorx@gmail.com>
 */
class albaran_cliente extends FacturaScripts\model\albaran_cliente
{
	public $iddevolucion;

    public function __construct($data = FALSE)
    {
        parent::__construct($data);

        if ($data) {
            $this->iddevolucion = $data['iddevolucion'];       
        } else {
            $this->iddevolucion = NULL;
        }
    }

    public function save()
    {
        if (parent::save()) {
            $sql = "UPDATE " . $this->table_name . " SET iddevolucion = " 
            . $this->var2str($this->iddevolucion) . "  WHERE idalbaran = " 
            . $this->var2str($this->idalbaran) . ";";

            return $this->db->exec($sql);
        }

        return FALSE;
    }

    public function returns_from_albaran()
    {
    	$devoluciones = array();
    	$sql = "SELECT referencia, SUM(cantidad) AS cantidad FROM lineasalbaranescli "
    		. "INNER JOIN albaranescli WHERE lineasalbaranescli.idalbaran = albaranescli.idalbaran "
            . "AND iddevolucion = " . $this->var2str($this->idalbaran) . " GROUP BY referencia;";

        $data = $this->db->select($sql);
        if ($data) {
            foreach ($data as $d) {
                $devoluciones[$d['referencia']] = $d['cantidad'];
            }
        }

        return $devoluciones;
    }

	public function _totales()
	{
		$totales = array('total' => 0.0, 'totaliva' => 0.0);
		$lineas = $this->get_lineas();

		foreach ($lineas as $linea) {
			$totales['total'] += $linea->pvptotal;
			$totales['totaliva'] += $linea->pvptotal * ($linea->iva - $linea->irpf + $linea->recargo) / 100;
		}

		return $totales;
	}

	public function _total()
	{
		$total = 0;
		$lineas = $this->get_lineas();

		foreach ($lineas as $linea) {
			$total+= $linea->pvptotal;
		}

		return $total;
	}

	public function _totaliva()
	{
		$total = 0.0;
		$lineas = $this->get_lineas();

		foreach ($lineas as $linea) {
			$total += $linea->pvptotal * ($linea->iva - $linea->irpf + $linea->recargo) / 100;
		}

		return $totales;
	}    
}
