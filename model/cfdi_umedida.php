<?php

/**
 * Description of ______________
 *
 * @author ________
 */

class cfdi_umedida extends fs_model {

    public $clave;
    public $descripcion;
    public $id;

    public function __construct($data = FALSE) {
        parent::__construct('cfdi_umedida');

        if ($data) {
            $this->load_from_data($data);
        } else {
            $this->clear();
        }
    }

    public function clear() {
        $this->clave = '';
        $this->descripcion = '';
        $this->id = NULL;        
    }

    public function load_from_data($data) {
        $this->clave = $data['clave'];
        $this->descripcion = $data['descripcion'];
        $this->id = $data['id'];        
    }

    public function install() {
        return '';
    }

    public function save() {
        if ($this->exists()) {
            $sql = "UPDATE " . $this->table_name . " SET "
            . "clave = " . $this->var2str($this->clave)
            . ", descripcion = " . $this->var2str($this->descripcion)
            . "  WHERE id = " . $this->var2str($this->id) . ";";
        } else {
            $sql = "INSERT INTO " . $this->table_name 
            . " (clave,descripcion) VALUES ("
            . $this->var2str($this->clave)
            . "," . $this->var2str($this->descripcion). ");";
        }        

        if ($this->db->exec($sql)) {
            $this->id = $this->db->lastval();
            return TRUE;
        }

        return FALSE;
    }

    public function get($id) {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE id = "
        . $this->var2str($id) . ";";

        $data = $this->db->select($sql);

        if ($data) {
            return new cfdi_umedida($data[0]);
        }
        return FALSE;
    }

    public function get_by_clave($clave) {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE clave = "
        . $this->var2str($clave) . ";";

        $data = $this->db->select($sql);

        if ($data) {
            return new cfdi_umedida($data[0]);
        }
        return FALSE;
    }

    public function all() {       
        $sql = "SELECT * FROM " . $this->table_name . " ORDER BY clave;";
        $data = $this->db->select($sql);

        if ($data) {
            $umedida = array();
            foreach ($data as $d) {
                $umedida[] = new cfdi_umedida($d);                
            }
        }
        return $umedida;
    }

    public function exists() {
        $sql = "SELECT * FROM " . $this->table_name . " WHERE clave = "
        . $this->var2str($this->clave) . ";";

        $data = $this->db->select($sql);

        if ($data) {
            $this->id = $data[0]['id'];
            return TRUE;
        }
        return FALSE;
    }

    public function delete() {

    }

}
