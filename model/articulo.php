<?php
/*
 * This file is part of facturacion_base
 * Copyright (C) 2012-2017  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

require_once 'plugins/facturacion_base/model/core/articulo.php';

/**
 * Almacena los datos de un artículo.
 *
 * @author Carlos García Gómez <neorazorx@gmail.com>
 */
class articulo extends FacturaScripts\model\articulo
{
    public $claveunidad;
    public $nombreunidad;

    public function __construct($data = false)
    {
        parent::__construct($data);
        $this->claveunidad = 'H87';
        $this->nombreunidad = 'Pieza';
    }

    public function clavesat()
    {
        if ($this->codfamilia) {
            $sql = "SELECT clavesat FROM familias WHERE codfamilia = " . $this->var2str($this->codfamilia) . ";";
            $data = $this->db->select($sql);

            if ($data) {
                return $data[0]['clavesat'];
            }
        }

        return '01010101';
    }

    public function claveum()
    {
        $sql = "SELECT claveum FROM articulo_um WHERE articuloref = " . $this->var2str($this->referencia) . ";";
        $data = $this->db->select($sql);

        if ($data) {
            return $data[0]['claveum'];
        } else {
            return $this->claveunidad;
        }
    }

    public function nombreum()
    {
        $sql = "SELECT nombreum FROM articulo_um WHERE articuloref = " . $this->var2str($this->referencia) . ";";
        $data = $this->db->select($sql);

        if ($data) {
            return $data[0]['nombreum'];
        } else {
            return $this->nombreunidad;
        }
    }
}
