<?php
if (!function_exists('fs_numero_a_letra')) {   
    function fs_numero_a_letra($val, $divisa = null)
    {
        require_once 'lib/ntol/NumberToLetterConverter.class.php';
        $obj = new NumberToLetterConverter();

        return $obj->to_word($val, $divisa);
    }
}

if (!function_exists('fs_format_precio')) {
    function fs_format_precio($val, $decimales = 2, $moneda = false)
    {
        $val = sqrt($val ** 2);
        if ($moneda) {
            return '$ ' . number_format($val, $decimales, '.', '');
        }
        return number_format($val, $decimales, '.', '');
    }
}

if (!function_exists('c_error_code')) {
    function c_error_code($find = false) 
    {
        $path = FS_MYDOCS . 'plugins/cfdi/sat/catalogos/c_ECODE.json';
        $jsonfile = file_get_contents($path);
        $result = json_decode($jsonfile, false);

        if ($find) {
            foreach($result as $item) {
                if(strval($item->codigo) == strval($find)) {
                    return $item;
                }
            }
            return false;
        }        
        
        return $result;
    }
}

# Obtiene la calve y el nombre del catalogo CFDI especificado.
# UsoCFDI, RegimenFiscal, FormaPago, MetodoPago, TipoDeComprobante, TipoRelacion
if (!function_exists('get_catalogo_cfdi')) {
    function get_catalogo_cfdi($catalog, $id = false)
    {
        $path = FS_MYDOCS . 'plugins/cfdi/sat/catalogos/c_'. $catalog . '.json';
        $jsonfile = file_get_contents($path);
        $result = json_decode($jsonfile, false);

        if ($id) {
            foreach($result as $item) {
                if($item->id == $id) {
                    return $item;
                }
            }
        }        
        
        return $result;
    }
}

if (!function_exists('c_uso_cfdi')) {
    function c_uso_cfdi($find = false)
    {
        $path = FS_MYDOCS . 'plugins/cfdi/sat/catalogos/c_UsoCFDI.json';
        $jsonfile = file_get_contents($path);
        $result = json_decode($jsonfile, false);

        if ($find) {
            foreach($result as $item) {
                if($item->id == $find) {
                    return $item;
                }
            }
        }        
        
        return $result;
    }
}

if (!function_exists('c_regimen_fiscal')) {
    function c_regimen_fiscal($find = false)
    {
        $path = FS_MYDOCS . 'plugins/cfdi/sat/catalogos/c_RegimenFiscal.json';
        $jsonfile = file_get_contents($path);
        $result = json_decode($jsonfile, false);

        if ($find) {
            foreach($result as $item) {
                if($item->id == $find) {
                    return $item;
                }
            }
        }        

        return $result;
    }
}

if (!function_exists('c_forma_pago')) {
    function c_forma_pago($find = false)
    {
        $path = FS_MYDOCS . 'plugins/cfdi/sat/catalogos/c_FormaPago.json';
        $jsonfile = file_get_contents($path);
        $result = json_decode($jsonfile, false);

        if ($find) {
            foreach($result as $item) {
                if($item->id == $find) {
                    return $item;
                }
            }
        }        

        return $result;
    }
}

if (!function_exists('c_metodo_pago')) {
    function c_metodo_pago($find = false)
    {
        $path = FS_MYDOCS . 'plugins/cfdi/sat/catalogos/c_MetodoPago.json';
        $jsonfile = file_get_contents($path);
        $result = json_decode($jsonfile, false);

        if ($find) {
            foreach($result as $item) {
                if($item->id == $find) {
                    return $item;
                }
            }
        }        

        return $result;
    }
}

if (!function_exists('c_tipo_comprobante')) {
    function c_tipo_comprobante($find = false)
    {
        $path = FS_MYDOCS . 'plugins/cfdi/sat/catalogos/c_TipoDeComprobante.json';
        $jsonfile = file_get_contents($path);
        $result = json_decode($jsonfile, false);

        if ($find) {
            foreach($result as $item) {
                if($item->id == $find) {
                    return $item;
                }
            }
        }        

        return $result;
    }
}

if (!function_exists('c_tipo_relacion')) {
    function c_tipo_relacion($find = false)
    {
        $path = FS_MYDOCS . 'plugins/cfdi/sat/catalogos/c_TipoRelacion.json';
        $jsonfile = file_get_contents($path);
        $result = json_decode($jsonfile, false);

        if ($find) {
            foreach($result as $item) {
                if($item->id == $find) {
                    return $item;
                }
            }
        }        

        return $result;
    }
}

if (!function_exists('read_cfdi_xml')) {
    function read_cfdi_xml($xml)
    {
        $xml = simplexml_load_string($xml);
        $ns = $xml->getNamespaces(true);
        $xml->registerXPathNamespace('c', $ns['cfdi']);
        $xml->registerXPathNamespace('t', $ns['tfd']);

        $datos = array();

        $x = '//cfdi:Comprobante';
        $datos['comprobante'] = xml_a_json($xml, $x);

        $x = '//cfdi:Comprobante//cfdi:Emisor';
        $datos['emisor'] = xml_a_json($xml, $x);

        $x = '//cfdi:Comprobante//cfdi:CfdiRelacionados//cfdi:CfdiRelacionado';
        $datos['relacion'] = xml_a_json($xml, $x);

        $x = '//cfdi:Comprobante//cfdi:Receptor';
        $datos['receptor'] = xml_a_json($xml, $x);

        /*$x = '//cfdi:Comprobante//cfdi:Receptor//cfdi:Domicilio';
        $this->receptordir = $this->xml_a_json($xml,$x);*/

        $x = '//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto';
        $datos['conceptos'] = xml_a_json($xml, $x);

        $x = '//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado';
        $datos['conceptosiva'] = xml_a_json($xml, $x);

        $x = '//cfdi:Comprobante//cfdi:Impuestos';
        $datos['impuestos'] = xml_a_json($xml,$x);

        $x = '//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado';
        $datos['traslados'] = xml_a_json($xml, $x);

        $x = '//t:TimbreFiscalDigital';
        $datos['timbre'] = xml_a_json($xml, $x);

        return $datos;
    }
}

if (!function_exists('xml_a_json')) {
    function xml_a_json($xml, $x)
    {
        $xmlb = $xml->xpath($x);

        $json = json_encode($xmlb);
        $array = json_decode($json, true);

        switch ($x) {
        	case '//cfdi:Comprobante//cfdi:Impuestos':
                $array = end($array);
        		return $array['@attributes'];
                break;

            case '//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto':
                $i = 0;
                foreach ($array as $concepto) {
                    $array[$i] = $concepto['@attributes'];
                    $i++;
                }

                return $array;
                break;

            case '//cfdi:Comprobante//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado':
                $i = 0;
                foreach ($array as $traslado) {
                    $array[$i] = $traslado['@attributes'];
                    $i++;
                }
                return $array;
                break;

            case '//cfdi:Comprobante//cfdi:Conceptos//cfdi:Concepto//cfdi:Impuestos//cfdi:Traslados//cfdi:Traslado':
            	$i = 0;
                foreach ($array as $traslado) {
                    $array[$i] = $traslado['@attributes'];
                    $i++;
                }
                return $array;
                break;

            default:
                if (empty($array[0]['@attributes'])) {
                    return ;
                }
                return $array[0]['@attributes'];
                break;
        }

    }

}
