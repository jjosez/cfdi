<?php

/*
 * @author XXXX      xxx@xxx.xx
 * @copyright 2016, XXXX. All Rights Reserved.
 */

/**
 * Description of facturacion_cfdi
 *
 * @author xxx
 */
require_once 'plugins/cfdi/lib/CFDI/autoload.php';

use XmlResourceRetriever\XsltRetriever;

class cfdi_home extends fs_controller
{
    public $cliente_dir = null;
    public $cliente_prop;
    public $cliente;
    public $pais;
    public $cfdi_um;
    public $settings;

    public $pendientes;
    public $local_xslt_path = 'tmp/' . FS_TMP_NAME . 'cfdi/sat/cache';

    public function __construct()
    {
        parent::__construct(__CLASS__, 'Configuracion', 'Facturacion');
    }

    protected function private_core()
    {
        $this->share_extensions();
        $this->get_settings();

        $this->pais = new pais();
        $this->cfdi_um = new cfdi_umedida();

        $get = filter_input(INPUT_GET, 'get');

        if ($get) {
            switch ($get) {
                case 'cliente':
                    $this->get_cfdi_cliente_settings();
                    break;

                case 'pendientes':
                    $this->get_cfdi_pendiente_timbrar();
                    break;

                case 'xslt':
                    $this->download_xslt_resouces();
                    break;

                default:
                    # code...
                    break;
            }
            return;
        }

        $save = filter_input(INPUT_POST, 'save');

        if ($save) {
            switch ($save) {
                case 'direccion':
                    $this->save_cliente_direccion_cfdi();
                    break;

                case 'settings':
                    $this->save_settings();
                    break;

                case 'um':
                    $this->save_articulo_cfdi_um();
                    break;

                case 'usocfdi':
                    $this->save_cliente_uso_cfdi();
                    break;
                
                default:
                    # code...
                    break;
            }
        }

        $this->get_cer_key_files();
    }

    private function download_xslt_resouces()
    {
        $retriever = new XsltRetriever($this->local_xslt_path);
        $remote = 'http://www.sat.gob.mx/sitio_internet/cfd/3/cadenaoriginal_3_3/cadenaoriginal_3_3.xslt';
        $local = $retriever->buildPath($remote);

        if (!file_exists($local)) {
            $retriever->retrieve($remote);
        }

        $this->new_message("Terminado : " . $local);
    }

    private function get_settings()
    {
        $fsvar = new fs_var();
        $this->settings = array(
            'cfdi_razon' => '',
            'cfdi_rfc' => '',
            'cfdi_dir' => '',
            'cfdi_dir_num' => '',
            'cfdi_dir_col' => '',
            'cfdi_dir_mun' => '',
            'cfdi_dir_loc' => '',
            'cfdi_dir_cp' => '',
            'cfdi_dir_edo' => '',
            'cfdi_dir_pais' => '',
            'cfdi_regimen' => '',
            'cfdi_num_cert' => '',
            'cfdi_cer' => false,
            'cfdi_key' => false,
            'cfdi_key_pass' => '',
            'cfdi_user_timbre' => '',
            'cfdi_pass_timbre' => '',
            'cfdi_prueba' => false,
            'cfdi_notimbrar' => false,
        );
        $this->settings = $fsvar->array_get($this->settings, false);
    }

    private function get_cfdi_cliente_settings()
    {
        $this->template = 'tab/cliente_cfdi_settings';
        $cod = filter_input(INPUT_GET, 'cod');                

        if ($cod) {
            $this->cliente = (new cliente)->get($cod);
            $this->cliente_dir = (new direccion_cliente)->get_billing_address($cod);
            $this->cliente_prop = (new cliente_propiedad)->get($cod,'usocfdi');
        }
    }

    private function get_cfdi_pendiente_timbrar()
    {
        $this->template = 'cfdi_pendientes';
        $this->page->title = 'Pendientes por timbrar';
        $this->pendientes = (new factura_cliente)->all_ptecfdi();
    }

    private function get_cer_key_files()
    {
        $keypath = $this->settings['cfdi_key'];
        $cerpath = $this->settings['cfdi_cer'];
        $numcert = $this->settings['cfdi_num_cert'];
        
        if (!is_null($numcert)) {
            if (!file_exists($keypath)) {
                $this->new_error_msg('Error al cargar configurcion. ' . $this->settings['cfdi_key']);
                return false;
            }

            if (!file_exists($cerpath)) {
                $this->new_error_msg('Error al cargar configurcion. ' . $this->settings['cfdi_cer']);
                return false;
            }
        } else {
            $this->new_error_msg('Error al cargar configuracion. ' . $numcert);
            return false;
        }

        return true;
    }


    private function save_articulo_cfdi_um()
    {
        if ($this->duplicated_petition($_POST['petition_id'])) {
            $this->new_error_msg('Petición duplicada. Has hecho doble clic sobre el botón Guardar
               y se han enviado dos peticiones.');
        } else {
            $this->cfdi_um->clave = $_POST['clave_um'];
            $this->cfdi_um->descripcion = $_POST['descrip_um'];

            if ($this->cfdi_um->save()) {
                $this->new_message('Nueva unidad de medida guardada correctamente.');
            } else {
                $this->new_error_msg('Error al guardar la nueva unidad de medida: ' .  $_POST['clave_um']);
            }
        }
    }

    private function save_cer_key_files()
    {
        $ruta = 'tmp/' . FS_TMP_NAME . 'cfdi/certificados';

        if (!file_exists($ruta)) {
            @mkdir($ruta, 0777, true);
        }

        if (is_uploaded_file($_FILES['cfdi_cer']['tmp_name'])) {
            copy($_FILES['cfdi_cer']['tmp_name'], $ruta . "/" .
                basename($_FILES["cfdi_cer"]["name"]));
            $this->settings['cfdi_cer'] = $ruta . "/" .
            basename($_FILES["cfdi_cer"]["name"]);
            $this->new_message('Archivo .cer cargado correctamente.');
        }

        if (is_uploaded_file($_FILES['cfdi_key']['tmp_name'])) {
            copy($_FILES['cfdi_key']['tmp_name'], $ruta . "/" .
                basename($_FILES["cfdi_key"]["name"]));
            $this->settings['cfdi_key'] = $ruta . "/" .
            basename($_FILES["cfdi_key"]["name"]);
            $this->new_message('Archivo .key cargado correctamente.');
        }
    }

    private function save_cliente_direccion_cfdi()
    {
        $this->template = 'tab/cliente_cfdi_settings';
        $dir = new direccion_cliente();

        if ('' != $_POST['coddir']) {
            $dir = $dir->get($_POST['coddir']);
        }

        $dir->apartado = $_POST['apartado'];
        $dir->ciudad = $_POST['ciudad'];
        $dir->codcliente = $_POST['codcliente'];
        $dir->codpais = $_POST['pais'];
        $dir->codpostal = $_POST['codpostal'];
        $dir->descripcion = $_POST['descripcion'];
        $dir->direccion = $_POST['direccion'];
        $dir->domenvio = 0;
        $dir->domfacturacion = 1;
        $dir->provincia = $_POST['provincia'];

        $dir->municipio = $_POST['municipio'];
        $dir->numero_ext = $_POST['numero_ext'];
        $dir->numero_int = $_POST['numero_int'];
        $dir->colonia = $_POST['colonia'];

        if ($dir->save()) {
            $this->new_message("Dirección guardada correctamente.");
            $this->cliente_dir = $dir;
        } else {
            $this->new_message("¡Imposible guardar la dirección!");
        }
    }

    private function save_cliente_uso_cfdi()
    {
        $this->template = 'tab/cliente_cfdi_settings';
        $prop = new cliente_propiedad();

        $prop->name = 'usocfdi';
        $prop->codcliente = $_POST['codcliente'];
        $prop->text = (isset($_POST['usocfdi'])) ? $_POST['usocfdi'] : 'P01' ;        

        if ($prop->save()) {
            $this->new_message("Uso guardado correctamente.");
            $this->cliente_prop = $prop;
        } else {
            $this->new_message("¡Imposible guardar!");
        }
    }

    private function save_settings()
    {
        $fsvar = new fs_var();

        $this->settings['cfdi_razon'] = $_POST['cfdi_razon'];
        $this->settings['cfdi_rfc'] = $_POST['cfdi_rfc'];
        $this->settings['cfdi_dir'] = $_POST['cfdi_dir'];
        $this->settings['cfdi_dir_num'] = $_POST['cfdi_dir_num'];
        $this->settings['cfdi_dir_col'] = $_POST['cfdi_dir_col'];
        $this->settings['cfdi_dir_mun'] = $_POST['cfdi_dir_mun'];
        $this->settings['cfdi_dir_loc'] = $_POST['cfdi_dir_loc'];
        $this->settings['cfdi_dir_cp'] = $_POST['cfdi_dir_cp'];
        $this->settings['cfdi_dir_edo'] = $_POST['cfdi_dir_edo'];
        $this->settings['cfdi_dir_pais'] = $_POST['cfdi_dir_pais'];
        $this->settings['cfdi_regimen'] = $_POST['cfdi_regimen'];
        $this->settings['cfdi_num_cert'] = $_POST['cfdi_num_cert'];
        $this->settings['cfdi_user_timbre'] = $_POST['cfdi_user_timbre'];
        $this->settings['cfdi_pass_timbre'] = $_POST['cfdi_pass_timbre'];
        $this->settings['cfdi_key_pass'] = $_POST['cfdi_key_pass'];

        $prueba = isset($_POST['cfdi_prueba']);
        $this->settings['cfdi_prueba'] = ($prueba) ? $prueba : 0 ;

        $timbrar = isset($_POST['c_notimbrar']);
        $this->settings['cfdi_notimbrar'] = ($timbrar) ? $timbrar : 0 ;

        $this->save_cer_key_files();

        if ($fsvar->array_save($this->settings)) {
            $this->new_message('Datos guardados correctamente.');
        } else {
            $this->new_error_msg('Error al guardar los datos.');
        }
    }

    private function share_extensions()
    {
        $extensiones = array(
            array(
                'name' => 'fileimputstyle',
                'page_from' => __CLASS__,
                'page_to' => null,
                'type' => 'head',
                'text' => '<script type="text/javascript" src="plugins/cfdi/view/js/bootstrap-filestyle.min.js"> </script>',
                'params' => ''
            ), array(
                'name' => 'cfdi_xslt_files',
                'page_from' =>  __CLASS__,
                'page_to' => __CLASS__,
                'type' => 'button',
                'text' => '<i class="fa fa-list" aria-hidden="true"></i>'
                . '<span class="hidden-xs">&nbsp;Bajar XSLT</span>',
                'params' => '&get=xslt'
            ), array(
                'name' => 'cfdi_cliente_data',
                'page_from' => __CLASS__,
                'page_to' => 'ventas_cliente',
                'type' => 'tab',
                'text' => '<span class="glyphicon glyphicon-qrcode" aria-hidden="true"></span> &nbsp; CFDI Extras',
                'params' => '&get=cliente'
            ), array(
                'name' => 'cfdi_factura',
                'page_from' => 'cfdi_nuevo',
                'page_to' => 'ventas_factura',
                'type' => 'button',
                'text' => '<i class="fa fa-file-code-o" aria-hidden="true"></i>'
                . '<span class="hidden-xs">&nbsp; CFDI</span>',
                'params' => ''
            ), array(
                'name' => 'cfdi_pendientes',
                'page_from' =>  __CLASS__,
                'page_to' => 'cfdi_lista',
                'type' => 'button',
                'text' => '<i class="fa fa-list" aria-hidden="true"></i>'
                . '<span class="hidden-xs">&nbsp;Pendientes</span>',
                'params' => '&get=pendientes'
            )
        );

        # añadimos/actualizamos las extensiones
        foreach ($extensiones as $ext) {
            $fsext = new fs_extension($ext);
            if (!$fsext->save()) {
                $this->new_error_msg('Imposible guardar los datos de la extensión ' . $ext['name'] . '.');
            }
        }

        # eliminamos las que sobran
        $fsext = new fs_extension();
        foreach ($fsext->all_from(__CLASS__) as $ext) {
            $encontrada = FALSE;
            foreach ($extensiones as $ext2) {
                if ($ext->name == $ext2['name']) {
                    $encontrada = TRUE;
                    break;
                }
            }

            if (!$encontrada) {
                $ext->delete();
            }
        }
    }
}
