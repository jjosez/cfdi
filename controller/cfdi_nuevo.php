<?php

/*
 * @author XXXX      xxx@xxx.xx
 * @copyright 2016, XXXX. All Rights Reserved.
 */

/**
 * Description of facturacion_cfdi
 *
 * @author xxx
 */

require_once 'plugins/cfdi/lib/CFDI/autoload.php';

require_once 'plugins/cfdi/lib/cfdi_ws.php';
require_once 'plugins/cfdi/lib/cfdi_document.php';
require_once 'plugins/cfdi/lib/cfdi_document_pdf.php';
require_once 'plugins/cfdi/lib/cfdi_document_reader.php';

use XmlResourceRetriever\XsltRetriever;

class cfdi_nuevo extends fs_controller
{
    public $factura;
    public $cfdi;
    public $usocfdi;
    public $rectificativa;
    
    public $xml;
    public $local_xslt_path = 'tmp/' . FS_TMP_NAME . 'cfdi/sat/cache';

    public $settings;

    public function __construct()
    {
        parent::__construct(__CLASS__, 'CFDI Nuevo', 'Facturacion', false, false);
    }

    protected function private_core()
    {
        $this->load_settings();

        $factura = filter_input(INPUT_GET, 'id');

        if ($factura) {
            $this->comprobar_factura_cfdi($factura, true);

            $this->factura = (new factura_cliente)->get($factura);

            if (!$this->factura) {
                $this->template = 'block/cfdi_error';
                $this->new_error_msg('Factura de cliente no encontrada');
                return;
            }

            if (!$this->factura->pagada) {
                $this->new_advice('No se soporta CFDI de pagos actualmente');
                return;
            }

            if ($this->verificar_facturas_pendientes($this->factura->fecha)) {
                return;
            }

            if ($this->factura->total < 0 && !$this->factura->idfacturarect) {
                $this->template = 'block/factura_rectificativa';
                $this->rectificativa = true;

                if (!isset($_POST['rectificativa'])) {                    
                    //$this->new_advice('Necesitas seleccionar una factura a la cual aplicarle la nota de credito');
                    return;
                }                 
            }

            $this->usocfdi = (new cliente_propiedad)->get($this->factura->codcliente,'usocfdi');

            $opcion = filter_input(INPUT_POST, 'opcion');
            if ($opcion) {
                switch ($opcion) {
                    case 'asignarnc':
                        $this->asignar_factura_rectificativa();
                        break;
                    case 'timbrar':
                        $this->generar_nuevo_cfdi($factura);
                        break;
                    
                    default:
                        # code...
                        break;
                }                
            }
        } 
    }

    private function asignar_factura_rectificativa()
    {
        $codigo = filter_input(INPUT_POST, 'cRectificativa');

        if ($codigo) {
            $frectificada = (new factura_cliente)->get_by_codigo($codigo);

            if ($frectificada) {

                if ($this->factura->codcliente != $frectificada->codcliente) {
                    $this->new_advice('No se puede relacionar la nota de credito pertenece a otro cliente.');
                    return;
                }

                $this->factura->idfacturarect = $frectificada->idfactura;
                $this->factura->codigorect = $frectificada->codigo;
                $this->factura->fecha = $frectificada->fecha;
                $this->factura->hora = date('H:i:s', strtotime('+5 seconds', strtotime($frectificada->hora)));
                $this->new_message('TOTAL DE LA FACTURA A RECT: ' . $frectificada->total);
                if ($this->factura->save()) {
                    $this->template = 'cfdi_nuevo';
                    $this->new_message('Nota de creito asignada correctamente a ' . $frectificada->codigo);                    
                    return;
                }
            }
            $this->new_advice('Factura relacionada no encontrada.');
        }
        //$this->factura->codcliente
    }

    private function comprobar_factura_cfdi($factura, $redireccionar = false)
    {
        if ($factura) {
            $cfdi = (new cfdi)->get_from_factura($factura);

            if ($cfdi) {
                if ($redireccionar) {
                    header('Location: ' . $cfdi->url());
                } else {
                    $this->new_advice("La factura ya se encuentra timbrada.");
                    $this->new_advice('Con UUID <a href="'.  $cfdi->url() . '">' 
                        . strtoupper($cfdi->uuid) . '</a>');
                    return true;
                }                               
            }
        } else {
            $this->new_error_msg("Error al cargar la factura.");
        }
        
        return false;
    }

    private function generar_nuevo_cfdi($factura)
    {
        $timbrar = $this->settings['cfdi_notimbrar'];
        $xml = $this->generar_cfdi_xml($this->factura);

        if (!$xml) {
            $this->new_advice('No se ha podido generar el XML.');
            return;
        }

        if (!$timbrar) {
            if ($this->timbrar_cfdi_xml($xml)) {
                if ($this->guardar_comprobante_cfdi($xml)) {
                    header('Location: ' . $this->cfdi->url());
                }               
            }
        }

        $this->xml = htmlspecialchars($xml);
    }

    private function generar_cfdi_xml($factura)
    {
        $data = array();

        if (isset($_POST['cUsoCFDI'])) {
            $data['uso'] = $_POST['cUsoCFDI'];
        } else {
            $info['usocfdi'] = 'P01';
            $data['uso'] = 'P01';
        }
        
        # Verificamos si tiene asignada una nota de credito
        if ($factura->idfacturarect) {
            $cfdi0 = new cfdi();
            $this->cfdi = $cfdi0->get_from_factura($factura->idfacturarect);

            if (!$this->cfdi) {
                $this->new_advice("Primero debe generar el CFDI del la factura, despues la factura de devolucion.");
                return false;
            }

            $data['tipo'] = 'egreso';
            $data['uso'] = 'G02';
            $data['uuid'] = $this->cfdi->uuid;
        } else {            
            if (isset($_POST['fglobal'])) {
                $data['tipo'] = 'global'; 
            } else {
                $data['tipo'] = 'ingreso';
            }         
        }

        $data['expedicion'] = $this->settings['cfdi_dir_cp'];
        $data['rfc'] = $this->settings['cfdi_rfc'];
        $data['nombre'] = $this->settings['cfdi_razon'];
        $data['regimen'] = $this->settings['cfdi_regimen'];


        # Generar xml del cfdi con la informacion proporcionada.
        $comprobante = new cfdi_document($factura,$data,$this->local_xslt_path);
        $comprobante->setCertificado($this->settings['cfdi_cer']);
        $comprobante->setLlave(file_get_contents($this->settings['cfdi_key']),'');

        return $comprobante->generarXml();
    }

    private function timbrar_cfdi_xml($xml)
    {
        $this->cfdi = new cfdi();

        $parametros = array();        
        $parametros['idComprobante'] = $this->factura->codigo;

        $prueba = ($this->settings['cfdi_prueba']) ? : false;

        if ($prueba) {
            $parametros['usuarioIntegrador'] = 'mvpNUXmQfK8=';
        } else {
            $parametros['usuarioIntegrador'] = $this->settings['cfdi_user_timbre'];
        }        

        $cliente = new cfdi_ws($parametros, $prueba, 1);
        
        /*if (!$cliente->test_ws()) {
            $this->new_error_msg("El Web Service no esta disponible!");
            return;
        }*/

        if ($cliente->timbrar($xml)) {
            $this->cfdi->uuid = $cliente->uuid;
            $this->cfdi->xml = $cliente->xml;
            $this->cfdi->cadenasat = $cliente->cadena_original;

            $this->new_message('Timbrado exitoso.');
            return true;
        } else {
            $error = c_error_code($cliente->codigo_error);            
            $edescripcion = $cliente->error;

            if ($error) {
                $edescripcion .= ' - ' . $error->descripcion;
            }

            $this->new_error_msg("ERROR[" . $cliente->codigo_error . "]: " . $edescripcion);
            return false;
        }
    }

    private function guardar_comprobante_cfdi($xml)
    {
        $cfdix = CfdiUtils\Cfdi::newFromString($xml);
        $cfdi = $cfdix->getNode();

        $emisor = $cfdi->searchNode('cfdi:Emisor');
        $receptor = $cfdi->searchNode('cfdi:Receptor');
        $tfd = $cfdi->searchNode('cfdi:Complemento', 'tfd:TimbreFiscalDigital');
        #$relacionados = $cfdi->searchNode('cfdi:CfdiRelacionados');
        $impuestos = $cfdi->searchNode('cfdi:Impuestos');
        $totalImpuestosTrasladados = 
            $cfdi->searchAttribute('cfdi:Impuestos', 'TotalImpuestosTrasladados');
        $totalImpuestosRetenidos = 
            $cfdi->searchAttribute('cfdi:Impuestos', 'TotalImpuestosRetenidos');

        $this->cfdi->codcliente = $this->factura->codcliente;
        $this->cfdi->idfactura = $this->factura->idfactura;
        $this->cfdi->folio = $cfdi['Folio'];
        $this->cfdi->version = $cfdi['Version'];
        /*if (null !== $relacionados){
            $uuids = '';
            foreach ($relacionados->searchNodes('cfdi:CfdiRelacionado') as $relacionado) {
                $uuids .= $relacionado['UUID'] . '|' .;
            }
            $this->cfdi->cfdirelacion = $uuids;
            $this->cfdi->tiporelacion = $relacionados['TipoRelacion'];
        } */       
        //uuid - cadenasat
        $this->cfdi->tipocomp = $cfdi['TipoDeComprobante'];
        $this->cfdi->condpago = "";
        $this->cfdi->metopago = $cfdi['MetodoPago'];
        $this->cfdi->formpago = $cfdi['FormaPago'];
        //fechamod
        $this->cfdi->fecha = $cfdi['Fecha'];
        $this->cfdi->subtotal = $cfdi['SubTotal'];
        $this->cfdi->descuento = $cfdi['Descuento'];
        $this->cfdi->iva = $totalImpuestosTrasladados;
        $this->cfdi->total = $cfdi['Total'];
        $this->cfdi->estado = 'emitido';

        $this->cfdi->rfcemisor = $emisor['Rfc'];
        $this->cfdi->rfcreceptor = $receptor['Rfc'];
        $this->cfdi->razonreceptor = $this->factura->nombrecliente;
        $this->cfdi->cpexpedicion = $cfdi['LugarExpedicion'];
        $this->cfdi->moneda = $cfdi['Moneda'];
        $this->cfdi->tipocambio = $cfdi['TipoCambio'];
        $this->cfdi->usocod = $receptor['UsoCFDI'];
        $this->cfdi->regimenemisor = $emisor['RegimenFiscal'];
        
        if ($this->cfdi->save()) {
            $this->factura->ptecfdi = FALSE;  
            $this->factura->save();         
            return true;
        }
        return false;        
    }

    private function cfdi_guardar_archivos()
    {
        if (!file_exists(FS_MYDOCS . 'plugins/cfdi/sat/comprobantes')) {
            @mkdir(FS_MYDOCS . 'plugins/cfdi/sat/comprobantes', 0777, true);
        }

        $comprobante = FS_MYDOCS . 'plugins/cfdi/sat/comprobantes' . $cliente->uuid;

        if ($cliente->xml) {
            file_put_contents($comprobante . ".xml", $cliente->xml);
        }

        if (isset($cliente->cadena_original)) {
            file_put_contents($comprobante . ".txt", $cliente->cadena_original);
        }

        if (isset($cliente->codigo_qr)) {
            file_put_contents($comprobante . ".jpg", $cliente->codigo_qr);
        }

        $this->new_message("Archivos cfdi guardados correctamente");
    }

    private function load_settings()
    {
        $fsvar = new fs_var();

        $this->settings = array
            (
            'cfdi_razon' => '', 'cfdi_rfc' => '', 'cfdi_dir' => '',
            'cfdi_dir_num' => '', 'cfdi_dir_col' => '', 'cfdi_dir_mun' => '',
            'cfdi_dir_loc' => '', 'cfdi_dir_cp' => '', 'cfdi_dir_edo' => '',
            'cfdi_dir_pais' => '', 'cfdi_regimen' => '', 'cfdi_num_cert' => '',
            'cfdi_cer' => false, 'cfdi_key' => false, 'cfdi_key_pass' => '',
            'cfdi_user_timbre' => '', 'cfdi_pass_timbre' => '', 'cfdi_prueba' => false, 'cfdi_notimbrar' => false
        );

        $this->settings = $fsvar->array_get($this->settings, false);
    }

    private function verificar_cer_key()
    {
        $r_key = FS_MYDOCS . $this->settings['cfdi_key'];
        $r_cer = FS_MYDOCS . $this->settings['cfdi_cer'];
        $n_cer = $this->settings['cfdi_num_cert'];

        if (!is_null($n_cer)) {
            if (!file_exists(FS_MYDOCS . $r_key)) {
                $this->new_error_msg('Error al cargar configurcion. ' . $r_key);
                return false;
            }

            if (!file_exists(FS_MYDOCS . $r_cer)) {
                $this->new_error_msg('Error al cargar configurcion. ' . $cer_key);
                return false;
            }
        } else {
            $this->new_error_msg('Error al cargar configurcion. ' . $n_cer);
            return false;
        }

        return true;
    }

    private function verificar_facturas_pendientes($date)
    {
        foreach ($this->factura->all_ptecfdi() as $fact) {
            $expectedDate = \DateTime::createFromFormat('d-m-y', $fact->fecha);
            $dateTo = \DateTime::createFromFormat('d-m-y', $date);
            //$now = new \DateTime();

            if ($expectedDate < $dateTo) { 
               $this->new_advice("Existen facturas pendientes por timbrar de dias anteriores");
               return true;
            } 
        }
        return false;
    }

    public function download_xslt_resouces()
    {
        $retriever = new XsltRetriever($this->local_xslt_path);
        $remote = 'http://www.sat.gob.mx/sitio_internet/cfd/3/cadenaoriginal_3_3/cadenaoriginal_3_3.xslt';
        $local = $retriever->buildPath($remote);

        if (!file_exists($local)) {
            $retriever->retrieve($remote);
        }

        $this->new_message("Terminado : " . $local);
    }

    private function format_fecha($f, $h)
    {
        $fecha_hora = date("Y-m-d", strtotime($f)) . 'T' . date("H:i:s", strtotime($h));
        return $fecha_hora;
    }

    private function format_precio($cantidad = 0, $decimales = 2)
    {
        $cantidad = sqrt($cantidad ** 2);
        return number_format($cantidad, $decimales, FS_NF1, '');
    }

    public function get_pago_desc($cod)
    {
        $formapago = new forma_pago();
        $fpago = $formapago->get($cod);
        return $fpago->descripcion;
    }
}
