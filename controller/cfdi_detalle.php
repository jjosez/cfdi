<?php

/*
 * @author XXXX      xxx@xxx.xx
 * @copyright 2018, XXXX. All Rights Reserved.
 */

/**
 * Description of facturacion_cfdi
 *
 * @author xxx
 */

require_once 'plugins/tcpdf/tcpdf_barcodes_2d.php';

require_once 'extras/phpmailer/class.phpmailer.php';
require_once 'extras/phpmailer/class.smtp.php';

require_once 'plugins/cfdi/lib/cfdi_ws.php';
require_once 'plugins/cfdi/lib/cfdi_document_reader.php';
require_once 'plugins/cfdi/lib/cfdi_document_pdf.php';

class cfdi_detalle extends fs_controller
{
    public $cfdi;
    public $xml;
    public $factura;
    private $settings;

    public function __construct()
    {
        parent::__construct(__CLASS__, 'comprobante', 'Facturacion', FALSE, FALSE);
    }

    protected function private_core()
    {
        $this->load_settings();

        /*..¿El usuario tiene permiso para eliminar la factura?..*/
        $this->allow_delete = $this->user->allow_delete_on('cfdi_detalle');

        if (isset($_GET['cfdi'])) {
            $this->cfdi = (new cfdi)->get($_GET['cfdi']);

            if ($this->cfdi) {
                $this->factura = (new factura_cliente)->get($this->cfdi->idfactura);

                $do = filter_input(INPUT_GET, 'do');

                if ($do) {
                    switch ($do) {
                        case 'CANCEL':
                            $this->cancelar_cfdi();
                            break;

                        case 'MAIL':
                            $this->enviar_email();
                            break;

                        case 'PDF':
                            $pdf = new cfdi_document_pdf($this->cfdi->xml);
                            $pdf->generar_pdf($this->cfdi->uuid, 'I');
                            break;

                        case 'XML':
                            $this->template = '';

                            header('Content-type: text/xml');
                            header('Content-Disposition: attachment; filename="' 
                                . strtoupper($this->cfdi->uuid) . '.xml"');

                            echo $this->cfdi->xml;
                            break;                        
                        
                        default:
                            $this->new_error_msg('Opcion invalida!');
                            break;
                    }
                    # code...
                }
                        
                if (!$this->factura->femail) {
                    $this->new_advice('Aun no se ha enviado el comprobante por email, puedes hacerlo con el boton enviar!');
                }

                $cfdi = new cfdi_document_reader($this->cfdi->xml);
                $this->xml['emisor'] = $cfdi->Emisor();
                $this->xml['receptor'] = $cfdi->Receptor();
                $this->xml['documento'] = $cfdi->Documento();
                $this->xml['conceptos'] = $cfdi->Conceptos();
                $this->xml['tfd'] = $cfdi->Tfd();
                $this->xml['relacionados'] = $cfdi->Relacionados();
                $this->xml['impuestos'] = $cfdi->Impuestos();
                $this->xml['tImpuestosTrasladados'] = $cfdi->TotalImpuestosTrasladados();
                $this->xml['tImpuestosRetenidos'] = $cfdi->TotalImpuestosRetenidos();

                //$this->xml = read_cfdi_xml($this->cfdi->xml);
            } else {
                $this->new_error_msg('CFDI no encontrado');
            }
        }
    }

    public function enviar_email()
    {
        $cliente0 = new cliente();
        $cliente = $cliente0->get($this->factura->codcliente);

        if (!$cliente->email) {
            $this->new_message('El cliente no tiene asignado ningun email. '
                . 'Puedes asignarle uno '
                . ' <a href="' . $cliente->url() . '" target="_blank" >aqui</a>');
            return;
        }

        $ruta = 'tmp/' . FS_TMP_NAME . 'enviar';

        if (!file_exists($ruta)) {
            mkdir($ruta);
        }

        $foliofactura = strtoupper($this->cfdi->uuid);
        $nombrearchivo = getcwd() . '/' . $ruta . '/' . $foliofactura;
        $nombrecliente = $this->factura->nombrecliente;

        $pdf = new cfdi_document_pdf($this->cfdi->xml);
        $pdf->generar_pdf($nombrearchivo, 'F');
        file_put_contents($nombrearchivo . '.xml', $this->cfdi->xml);

        if ($this->empresa->can_send_mail()) {
            if (file_exists($nombrearchivo . '.pdf')) {
                $mail = $this->empresa->new_mail();
                $mail->FromName = $this->empresa->nombrecorto;

                $mail->addAddress($cliente->email, $nombrecliente);
                $mail->Subject = 'Su factura ' . $foliofactura;

                $mensaje = 'Envio de su comprobante fiscal digital. Gracias por su preferencia =)';
                $mail->Body = $mensaje;

                $mail->addAttachment($nombrearchivo . '.pdf');
                $mail->addAttachment($nombrearchivo . '.xml');

                if ($this->empresa->mail_connect($mail) && $mail->send()) {
                    $this->new_message('Mensaje enviado correctamente.');

                    $this->factura->femail = $this->today();
                    $this->factura->save();

                    $this->empresa->save_mail($mail);
                } else {
                    $this->new_error_msg("Error al enviar el email: " . $mail->ErrorInfo);
                }

                unlink($nombrearchivo . '.pdf');
                unlink($nombrearchivo . '.xml');
            } else {
                $this->new_error_msg('Imposible generar el PDF.');
            }
        }
    }

    private function cancelar_cfdi()
    {
        $parametros = array();        
        $parametros['idComprobante'] = $this->factura->codigo;

        $prueba = ($this->settings['cfdi_prueba']) ? : false;

        if ($prueba) {
            $parametros['usuarioIntegrador'] = 'mvpNUXmQfK8=';
        } else {
            $parametros['usuarioIntegrador'] = $this->settings['cfdi_user_timbre'];
        }        

        $cliente = new cfdi_ws($parametros, $prueba, 1);
        
        /*if (!$cliente->test_ws()) {
            $this->new_error_msg("El Web Service no esta disponible!");
            return;
        }
        */
        if ($cliente->cancelar($this->cfdi->rfcemisor, $this->cfdi->uuid)) {

            $this->new_message($cliente->descripcion_resultado);
            $this->cfdi->cancelado = true;
            $this->cfdi->save();
            return true;
        } else {
            $error = c_error_code($cliente->codigo_error);            
            $edescripcion = $cliente->error;

            if ($error) {
                $edescripcion .= ' - ' . $error->descripcion;
            }

            $this->new_error_msg("ERROR[" . $cliente->codigo_error . "]: " . $edescripcion);
            $this->new_error_msg($cliente->descripcion_resultado);
            return false;
        }
    }

    public function generar_qr()
    {
        $texto = 'https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx';
        $texto = 'https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx';
        $texto .= '&id=' . $this->cfdi->uuid;
        $texto .= '&re=' . $this->cfdi->rfcemisor;
        $texto .= '&rr=' . $this->cfdi->rfcreceptor;
        $texto .= '&tt=' . $this->cfdi->total;
        $texto .= '&fe=' . substr($this->xml['tfd']['SelloSAT'], -8);

        $archivo = FS_MYDOCS . 'plugins/cfdi/sat/comprobantes/qr0.png';

        $qrcode = new TCPDF2DBarcode($texto, 'QRCODE,M');
        $img = $qrcode->getBarcodePngData(4.7, 4.7, array(0, 0, 0));

        if (file_put_contents($archivo, $img)) {
            return $archivo;
        }

        return false;
    }

    private function load_settings()
    {
        $fsvar = new fs_var();

        $this->settings = array
            (
            'cfdi_razon' => '', 'cfdi_rfc' => '', 'cfdi_dir' => '',
            'cfdi_dir_num' => '', 'cfdi_dir_col' => '', 'cfdi_dir_mun' => '',
            'cfdi_dir_loc' => '', 'cfdi_dir_cp' => '', 'cfdi_dir_edo' => '',
            'cfdi_dir_pais' => '', 'cfdi_regimen' => '', 'cfdi_num_cert' => '',
            'cfdi_cer' => false, 'cfdi_key' => false, 'cfdi_key_pass' => '',
            'cfdi_user_timbre' => '', 'cfdi_pass_timbre' => '', 'cfdi_prueba' => false, 'cfdi_notimbrar' => false
        );

        $this->settings = $fsvar->array_get($this->settings, false);
    }
}
