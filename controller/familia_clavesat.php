<?php
/*
 * This file is part of FacturaScripts
 * Copyright (C) 2013-2017  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of ______________
 *
 * @author ________
 */

class familia_clavesat extends fs_controller
{
    public $familia;

    public function __construct()
    {
        $this->icon = "fa-address-card";
        $this->title = "articulo_umedida";

        parent::__construct(__CLASS__, 'Familia Clavesat', 'Facturacion', FALSE, FALSE);
    }

    protected function private_core()
    {
        # configure delete action
        $this->allow_delete = $this->user->allow_delete_on(__CLASS__);
        $this->template = 'block/familia_clavesat';

        #Load data with estructure data
        parent::private_core();

        #Cargamos las extensiones
        $this->share_extensions();

        $this->familia = new familia();
        $fam0 = new familia();        

        if (isset($_GET['cod'])) {           
            $this->familia = $fam0->get($_GET['cod']);
        } elseif (isset($_POST['familia'])) {
            $this->familia = $fam0->get($_POST['familia']);
            $this->familia->clavesat = $_POST['clavesat'];

            $this->familia->save();
        }
    }

     private function share_extensions()
    {
        $extensiones = array(
            array(
                'name' => 'cfdi_clavesat',
                'page_from' => 'familia_clavesat',
                'page_to' => 'ventas_familia',
                'type' => 'tab',
                'text' => '<i class="fa fa-tag" aria-hidden="true"></i>'
                . '<span class="hidden-xs">&nbsp; SAT</span>',
                'params' => ''
            )
        );

        # añadimos/actualizamos las extensiones
        foreach ($extensiones as $ext) {
            $fsext = new fs_extension($ext);
            if (!$fsext->save()) {
                $this->new_error_msg('Imposible guardar los datos de la extensión ' . $ext['name'] . '.');
            }
        }

        # eliminamos las que sobran
        $fsext = new fs_extension();
        foreach ($fsext->all_from(__CLASS__) as $ext) {
            $encontrada = FALSE;
            foreach ($extensiones as $ext2) {
                if ($ext->name == $ext2['name']) {
                    $encontrada = TRUE;
                    break;
                }
            }

            if (!$encontrada) {
                $ext->delete();
            }
        }
    }
}
