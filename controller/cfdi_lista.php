<?php

/*
 * @author XXXX      xxx@xxx.xx
 * @copyright 2016, XXXX. All Rights Reserved.
 */

/**
 * Description of facturacion_cfdi
 *
 * @author xxx
 */
class cfdi_lista extends fs_list_controller
{
    public $cliente;

    public function __construct()
    {
        parent::__construct(__CLASS__, 'CFDIS', 'Facturacion');
        $this->template = 'cfdi_list';
    }

    protected function create_tabs()
    {
        $this->add_tab('cfdi', 'CFDIs', 'cfdi');
        $this->add_search_columns('cfdi',['rfcreceptor', 'razonreceptor', 'codcliente']);
        $this->add_sort_option('cfdi', ['fecha', 'folio'], 2);
        $this->add_sort_option('cfdi', ['razonreceptor']);
        $this->add_filter_date('cfdi', 'fecha', 'desde', '>=');
        $this->add_filter_date('cfdi', 'fecha', 'hasta', '<=');

        $tiposcomprobante = [ 'E' => 'Egreso', 'I' => 'Ingreso'];
        $this->add_filter_select('cfdi', 'tipocomp', 'tipo', $tiposcomprobante);
        $this->add_filter_checkbox('cfdi', 'cancelado', 'CFDIs Cancelados', '=', true);

        //foreach ((new cliente)->all() as $cliente) {
        //    $clientes[$cliente->cifnif] =  $cliente->nombre;
        //}
        //$this->add_filter_select('cfdi', tipocomp, 'Tipo de comprobante', $clientes);

        $this->decoration->add_row_option('cfdi', 'tipocomp', 'E', 'warning');
        $this->decoration->add_row_option('cfdi', 'cancelado', '1', 'danger');        
        $this->decoration->add_row_url('cfdi', '?page=cfdi_detalle&cfdi=', 'id');

        #columnas a mostrar
        $this->decoration->add_column('cfdi', 'folio', null,'Folio');
        $this->decoration->add_column('cfdi', 'razonreceptor', null, 'Cliente');
        $this->decoration->add_column('cfdi', 'uuid', null, 'UUID', 'text-uppercase');
        $this->decoration->add_column('cfdi', 'tipocomp', null, 'Tipo');
        $this->decoration->add_column('cfdi', 'total', 'money', 'Total');
        $this->decoration->add_column('cfdi', 'fecha', 'date', 'Fecha', 'text-right');
    }
}
