<?php
/*
 * This file is part of FacturaScripts
 * Copyright (C) 2013-2017  Carlos Garcia Gomez  neorazorx@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Description of ______________
 *
 * @author ________
 */

class articulo_umedida extends fs_controller
{
    public $articulo;
    public $articulo_um;
    public $cfdi_um;

    public function __construct()
    {
        $this->icon = "fa-address-card"; // PUT HERE YOUR CUSTOM ICON
        $this->title = "articulo_umedida";
// PUT HERE YOUR CUSTOM TITLE

        // run standard entry point
        parent::__construct(__CLASS__, 'Articulo UM', 'Facturacion', FALSE, FALSE); // PUT HERE MENU OPTION WHERE INSTALL CONTROLLER
    }

    protected function private_core()
    {
        // configure delete action
        $this->allow_delete = $this->user->allow_delete_on(__CLASS__);
        $this->template = 'block/articulo_um';

        parent::private_core(); // Load data with estructure data

        $this->articulo_um = new articulo_um();
        $this->cfdi_um = new cfdi_umedida();
        $this->articulo = false;
        $art0 = new articulo();
        $artum0 = new articulo_um();
        $cfdium0 = new cfdi_umedida();

        $this->shared_extensions();

        if (isset($_GET['ref'])) {
            $this->articulo = $art0->get($_REQUEST['ref']);
            $this->articulo_um = $artum0->get($_REQUEST['ref']);
            
        } elseif (isset($_POST['um'])) {
            $this->articulo = $art0->get($_POST['ref']);
            if ($this->articulo) {

                $this->cfdi_um = $cfdium0->get_by_clave($_POST['umclave']);
                $this->articulo_um->articuloref = $this->articulo->referencia;
                $this->articulo_um->claveum = $this->cfdi_um->clave;
                $this->articulo_um->nombreum = $this->cfdi_um->descripcion;

                if ($this->articulo_um->save()) {
                    $this->new_message('¡Unidad de medida agregada correctamente!');
                } else {
                    $this->new_error_msg('Ocurrio un error al tratar de agregar la unidad de medida, por favor revise los datos ingresados');
                }
            }
        }
    }

    public function shared_extensions(){
        $extensiones = array(
            array(
                'name' => 'articulo_um',
                'page_from' => __CLASS__,
                'page_to' => 'ventas_articulo',
                'type' => 'tab',
                'text' => '<span class="fa fa-cubes"></span>&nbsp;<span class="hidden-xs">&nbsp; Unidades de Medida</span>',
                'params' => ''
            )
        );

        # añadimos/actualizamos las extensiones
        foreach ($extensiones as $ext) {
            $fsext = new fs_extension($ext);
            if (!$fsext->save()) {
                $this->new_error_msg('Imposible guardar los datos de la extensión ' . $ext['name'] . '.');
            }
        }

        # eliminamos las que sobran
        $fsext = new fs_extension();
        foreach ($fsext->all_from(__CLASS__) as $ext) {
            $encontrada = FALSE;
            foreach ($extensiones as $ext2) {
                if ($ext->name == $ext2['name']) {
                    $encontrada = TRUE;
                    break;
                }
            }

            if (!$encontrada) {
                $ext->delete();
            }
        }
    }
}
