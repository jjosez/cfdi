<?php
/**
 * Description of ______________
 *
 * @author Juan Jose Prieto Dzul
 * @copyright 2016, XXXX. All Rights Reserved.
 */

ini_set("soap.wsdl_cache_enabled", 1);

class cfdi_ws
{
    public $ws_url = "https://cfdi33-pruebas.buzoncfdi.mx:1443/Timbrado.asmx?wsdl";
    //public $ws_url = "https://timbracfdi33.mx:1443/Timbrado.asmx";
    public $parametros = array();
    public $context;
    public $options;

    public $xml;
    public $uuid;
    public $codigo_qr;
    public $cadena_original;

    public $log = 'plugins/cfdi/log.txt';
    public $error;
    public $codigo_error;
    public $debug;
    public $error_interno;
    public $mensaje_interno;

    public function __construct($parametros = array(), $prueba, $debug = 0)
    {
        $this->debug = (int) $debug;        

        foreach ($parametros as $k => $v) {
            $this->parametros[$k] = $v;
        }

        if (!$prueba) { 
            #WS de produccion           
            $this->ws_url = "https://timbracfdi33.mx:1443/Timbrado.asmx?wsdl";
        }

        $this->context = stream_context_create(
            array(
                'ssl' => array(
                    // set some SSL/TLS specific options
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => $prueba, //--> solamente true en ambiente de pruebas
                ),
                'http' => array(
                    'user_agent' => 'PHPSoapClient',
                ),
            ));

        $this->options = array(
            'stream_context' => $this->context,
            'cache_wsdl' => WSDL_CACHE_MEMORY,
            'trace' => true,
            'exceptions' => 0,
        );
    }

    public function timbrar($cfdi)
    {
        libxml_disable_entity_loader(false);

        $parametros['xmlComprobanteBase64'] = base64_encode($cfdi);
        $parametros = array_merge($parametros, $this->parametros);

        $cliente = new SoapClient($this->ws_url, $this->options);
        $respuesta = $cliente->__soapCall('TimbraCFDI', array('parameters' => $parametros));

        if (!is_soap_fault($respuesta)) {
            $this->error = $respuesta->TimbraCFDIResult->anyType[0];
            $this->codigo_error = $respuesta->TimbraCFDIResult->anyType[1];
            $this->descripcion_resultado = $respuesta->TimbraCFDIResult->anyType[2];
            $this->xml = $respuesta->TimbraCFDIResult->anyType[3];
            $this->codigo_qr = $respuesta->TimbraCFDIResult->anyType[4];
            $this->cadena_original = $respuesta->TimbraCFDIResult->anyType[5];

            if ($this->xml != '') {
                $xml_cfdi = simplexml_load_string($this->xml);
                $xml_cfdi->registerXPathNamespace("tfd", "http://www.sat.gob.mx/TimbreFiscalDigital");
                $tfd = $xml_cfdi->xpath('//tfd:TimbreFiscalDigital');
                $this->uuid = (string) $tfd[0]['UUID'];

                return true;
            }
        } else {
            if ($this->debug == 1) {
                $this->log("ERROR:\t" . $respuesta->faultcode . " \t" . $respuesta->faultstring);
                $this->error = $respuesta->faultstring;
                $this->codigo_error = $respuesta->faultcode;
            }

            return false;
        }
    }

    public function cancelar($emisor, $UUID)
    {
        $parametros['usuarioIntegrador'] = $this->parametros['usuarioIntegrador'];
        $parametros['rfcEmisor'] = $emisor;
        $parametros['folioUUID'] = $UUID;

        $cliente = new SoapClient($this->ws_url, $this->options);
        $respuesta = $cliente->__soapCall('CancelaCFDI', array('parameters' => $parametros));

        if (!is_soap_fault($respuesta)) {
            $this->error = $respuesta->CancelaCFDIResult->anyType[0];
            $this->codigo_error = $respuesta->CancelaCFDIResult->anyType[1];
            $this->descripcion_resultado = $respuesta->CancelaCFDIResult->anyType[2];
            $this->xml = $respuesta->CancelaCFDIResult->anyType[3];
            $this->codigo_qr = $respuesta->CancelaCFDIResult->anyType[4];
            $this->cadena_original = $respuesta->CancelaCFDIResult->anyType[5];

            if ($this->codigo_error == '0') {
                return true;                
            }
            return false; 
        } else {
            if ($this->debug == 1) {
                $this->log("SOAP Error:\t" . $respuesta->faultcode . " Descripcion:\t" . $respuesta->faultstring);
                $this->error = $respuesta->faultstring;
                $this->codigo_error = $respuesta->faultcode;
            }
        }
        return false;
    }

    public function obtener()
    {
        $ws_respuesta = '';

        libxml_disable_entity_loader(false);

        try {
            $cliente = new SoapClient($this->ws_url, $this->options);
            $ws_respuesta = $cliente->__soapCall('ObtieneCFDI', array('parameters' => $this->parametros));

            $this->error = $ws_respuesta->ObtieneCFDIResult->anyType[0];
            $this->codigo_error = $ws_respuesta->ObtieneCFDIResult->anyType[1];
            $this->descripcion_resultado = $ws_respuesta->ObtieneCFDIResult->anyType[2];
            $this->xml = $ws_respuesta->ObtieneCFDIResult->anyType[3];
            $this->codigo_qr = $ws_respuesta->ObtieneCFDIResult->anyType[4];
            $this->cadena_original = $ws_respuesta->ObtieneCFDIResult->anyType[5];
            $this->error_interno = $ws_respuesta->ObtieneCFDIResult->anyType[6];
            $this->mensaje_interno = $ws_respuesta->ObtieneCFDIResult->anyType[7];

            if ($this->codigo_error == '0') {
                return true;
            }

        } catch (SoapFault $fault) {

            if ($this->debug == 1) {
                $this->log("SOAP Error:\t" . $e->faultcode . " Descripcion:\t" . $e->faultstring);
                $this->error = $e->faultstring;
                $this->codigo_error = $e->faultcode;
                echo "SOAPFault: " . $fault->faultcode . "-" . $fault->faultstring . "\n";
            }

        }

        return false;
    }

    public function test_ws($url = false) {
        $url = ($url) ? : $this->ws_url;

        $timeout = 10;
        $ch = curl_init();
        
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $ch, CURLOPT_TIMEOUT, $timeout );
        
        $response = curl_exec($ch);
        $response = trim( strip_tags( $response ) );
        $response_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
        curl_close( $ch );
        
        if (($response_code == "200") || ($response_code == "302")) {
            return true;
        }

        return false;
    }

    private function log($str)
    {
        $f = fopen($this->log, 'a');
        fwrite($f, date('c') . "\t" . $str . "\n\n");
        fclose($f);
    }
}
