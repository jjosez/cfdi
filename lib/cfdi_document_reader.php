<?php
/**
 * Description of ______________
 *
 * @author Juan Jose Prieto Dzul
 * @copyright 2016, XXXX. All Rights Reserved.
 */
require_once 'plugins/cfdi/lib/CFDI/autoload.php';

use \CfdiUtils\CfdiCreator33; 

class cfdi_document_reader
{
    private $emisor;
    private $receptor;
    private $documento;
    private $conceptos;
    private $tfd;
    private $relacionados;
    private $impuestos;
    private $totalImpuestosTrasladados;
    private $totalImpuestosRetenidos;
    private $cfdi;

    public function __construct($xml)
    {
        $cfdi0 = CfdiUtils\Cfdi::newFromString($xml);
        $this->cfdi = $cfdi0->getNode();
    }

    public function Emisor()
    {
        $this->emisor = $this->cfdi->searchNode('cfdi:Emisor');
        return $this->emisor;
    }

    public function Receptor()
    {
        $this->receptor = $this->cfdi->searchNode('cfdi:Receptor');
        return $this->receptor;
    }

    public function Documento()
    {
        $this->documento = $this->cfdi;
        return $this->documento;
    }

    public function Conceptos()
    {
        $this->conceptos = $this->cfdi->searchNodes('cfdi:Conceptos', 'cfdi:Concepto');
        return $this->conceptos;
    }

    public function Tfd()
    {
        $this->tfd = $this->cfdi->searchNode('cfdi:Complemento', 'tfd:TimbreFiscalDigital');
        return $this->tfd;
    }

    public function Relacionados()
    {
        $this->relacionados = $this->cfdi->searchNode('cfdi:CfdiRelacionados');
        return $this->relacionados;
    }

    public function Impuestos()
    {
        $this->impuestos = $this->cfdi->searchNode('cfdi:Impuestos');
        return $this->impuestos;
    }

    public function TotalImpuestosTrasladados()
    {
        $this->totalImpuestosTrasladados = $this->cfdi->searchAttribute('cfdi:Impuestos', 'TotalImpuestosTrasladados');
        return $this->totalImpuestosTrasladados;
    }

    public function TotalImpuestosRetenidos()
    {
        $this->totalImpuestosRetenidos = $this->cfdi->searchAttribute('cfdi:Impuestos', 'TotalImpuestosRetenidos');;
        return $this->totalImpuestosRetenidos;
    }

}
