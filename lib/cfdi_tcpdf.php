<?php

/*
 * @author XXXX      xxx@xxx.xx
 * @copyright 2016, XXXX. All Rights Reserved.
 */

/**
 * Description of CFDI_TCPDF
 *
 * @author xxx
 */

require_once 'plugins/tcpdf/tcpdf.php';

class CFDI_TCPDF extends TCPDF {
    private $emisor;
    private $receptor;
    private $documento;
    private $relacionados;
    private $tfd;

    protected $last_page_flag;

    function __construct($datos, $orientation, $unit, $format) {
        parent::__construct($orientation, $unit, $format, true, 'UTF-8', false);
        define('MARGENX', 10);
        define('MARGENY', 10);

        $permissions = array('modify', 'annot-forms', 'fill-forms', 'extract', 'assemble');

        $this->SetProtection($permissions,'', null, 0, null);
        $this->SetCreator(PDF_CREATOR);
        $this->SetMargins(MARGENX, MARGENY, MARGENX);
        //$this->SetHeaderMargin(PDF_MARGIN_HEADER);
        //$this->SetFooterMargin(PDF_MARGIN_FOOTER);
        # $this->pdf->SetAutoPageBreak(TRUE, 60);

        $this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $this->setCellPaddings(1, 1, 1, 1);
        $this->setCellMargins(1, 1, 1, 1);
        $this->SetFillColor(255, 255, 127);

        $this->emisor = $datos['emisor'];
        $this->receptor = $datos['receptor'];
        $this->documento = $datos['documento'];
        $this->relacionados = $datos['relacionados'];
        $this->tfd = $datos['tfd'];

        $this->SetTitle($this->documento['Folio']);

        define('ANCHO', $this->getPageWidth() - MARGENX*2);
        define('LIMITEX', $this->GetPageWidth() - MARGENX);
        define('LIMITEY', $this->GetPageHeight() - MARGENY);
    }


    public function Header() {
        $headerdata = $this->getHeaderData();
        $this->HeaderTemplate();

        $texto = $this->documento['Folio'];
        $this->texto(BOLD3, null, $texto, MARGENX, MARGENY, 0, 5);
        $texto = 'UUID: ' . $this->tfd['UUID'];
        $this->texto(BOLD3, null, strtoupper($texto), MARGENX, MARGENY, 0, 5, 'R');

        $texto = 'Lugar de expedición: '. $this->documento['LugarExpedicion'];
        $this->texto(NORM5, null, $texto, MARGENX, MARGENY+7, 100);
        $texto = 'Fecha de expedición: '. $this->documento['Fecha'];
        $this->texto(NORM5, null, $texto, MARGENX + 100, MARGENY+7, 0, 0, 'R');

        $Xpos = (ANCHO + MARGENX*2)/2 - $headerdata['logo_width']/2;
        $this->Image($headerdata['logo'], $Xpos, MARGENY + 17, $headerdata['logo_width']);
        $this->texto(NORM5, null, 'autopaisa_d@hotmail.com', MARGENX, MARGENY+35, 0, 5, 'C');
        $this->texto(NORM5, null, 'Tel: 985-856-5990', MARGENX, MARGENY+39, 0, 5, 'C');

        $this->print_factura_emisor();
        $this->print_factura_receptor();
        $this->print_factura_info();

        $this->print_plantilla_conceptos();
    }

    public function Footer() {
        $this->FooterTemplate();
    }

    public function Close() {
        $this->last_page_flag = true;
        parent::Close();
    }

    private function HeaderTemplate() {
        $this->Line(MARGENX, MARGENY + 12, ANCHO + MARGENX, MARGENY + 12);       
    }

    private function FooterTemplate() {       
        $texto = 'Pagina ' . $this->getAliasNumPage() .' / '. $this->getAliasNbPages();
        $this->texto(NORM5, null,$texto, MARGENX, LIMITEY-4, ANCHO, 4,'L');
        $texto = 'Este documento es la representación impresa de un CFDI.';
        $this->texto(NORM5, null,$texto, MARGENX, LIMITEY-4, 0, 4,'R');
    }

    public function print_factura_info()
    {
        if ($this->relacionados['TipoRelacion']) {
            $tipo = c_tipo_relacion($this->relacionados['TipoRelacion']);
            $texto = '<b>Tipo relacion:</b><br />' . $tipo->id . ' ' . $tipo->descripcion;
            $this->multitexto(NORM6, null, $texto, MARGENX + 140, MARGENY+30, 0, 5, 'R',true);

            $texto = '<b>CFDI Relacionado:</b><br />';
            foreach ($this->relacionados->searchNodes('cfdi:CfdiRelacionado') as $relacionado) {
                $texto .= strtoupper($relacionado['UUID']) . '<br />';
            }

            $this->multitexto(NORM6, null, $texto, MARGENX + 140, MARGENY+38, 0, 5, 'R',true);           
        }

        $texto = 'No de certificado: '. $this->documento['NoCertificado'] . "\n";
        $tipo = c_tipo_comprobante($this->documento['TipoDeComprobante']);
        $texto .= 'Tipo comprobante: ' .  $tipo->id . ' ' . $tipo->descripcion;

        $this->multitexto(NORM6, null, $texto, MARGENX+140, MARGENY+20, 0, 6, 'R');

        /*$texto = 'No de certificado: '. $this->documento['NoCertificado'];
        $texto .= ' | Tipo documento: '. $this->documento['TipoDedocumento'];
        $this->texto(NORM5, null,$texto, MARGENX, 60, 0, 5, 'R');*/
    }

    public function print_factura_emisor()
    {
        $texto = $this->emisor['Nombre'] . "\n";
        $texto .= $this->emisor['Rfc'] . "\n";
        $regimen = c_regimen_fiscal($this->emisor['RegimenFiscal']);
        $texto .= 'Regimen: ' . $regimen->id . ' - ' . $regimen->descripcion;

        $this->texto(BOLD5, null, 'Emisor:', MARGENX, MARGENY+15, 70, 5);        
        $this->multitexto(NORM6, null, $texto, MARGENX, MARGENY+20, 70, 6);
    }

    public function print_factura_receptor()
    {
        $usocfdi = c_uso_cfdi($this->receptor['UsoCFDI']);

        $texto = ($this->receptor['Nombre']) ? $this->receptor['Nombre'] . "\n" : '';
        $texto .= $this->receptor['Rfc'] . "\n";
        $texto .= 'Uso CFDI: '. $usocfdi->id . ' ' . $usocfdi->descripcion;

        $this->texto(BOLD5, null, 'Receptor:', MARGENX, MARGENY+35, 0, 5);        
        $this->multitexto(NORM6, null, $texto, MARGENX, MARGENY+40, 0, 6);
    }

    public function print_concepto_clave($texto,$posicion_y)
    {
        $this->texto(NORM6,null,$texto, MARGENX+1, $posicion_y, 13, 4,'C');
    }

    public function print_concepto_um($texto,$posicion_y)
    {
        $this->texto(NORM6,null,$texto, MARGENX+14, $posicion_y, 12, 4,'C');
    }

    public function print_concepto_unidad($val,$posicion_y)
    {
        $this->texto(NORM6,null,$val, MARGENX+26, $posicion_y, 12, 4,'C');
    }

    public function print_concepto_cantidad($val,$posicion_y)
    {
        $this->texto(NORM6,null,$val, MARGENX+38, $posicion_y, 12, 4,'C');
    }    

    public function print_concepto_descripcion($texto,$posicion_y)
    {
        $this->texto(NORM6,null,$texto, MARGENX+50, $posicion_y, 110, 4); 
    }

    public function print_concepto_precio($texto,$posicion_y)
    {
        $this->texto(NORM6,null,$texto, MARGENX+160, $posicion_y, 16, 4,'R');
    }

    public function print_concepto_importe($texto,$posicion_y)
    {
        $this->texto(NORM6,null,$texto, MARGENX+176, $posicion_y, 19, 4,'R');
    }

    public function print_factura_subtotal($texto,$posicion_y)
    {
        $this->texto(NORM5,null,'Subtotal:',ANCHO + MARGENX - 41, $posicion_y + 14, 20, 7,'R');
        $this->texto(NORM5,null,$texto, ANCHO + MARGENX - 21, $posicion_y + 14, 20, 7,'R');
    }

    public function print_factura_impuesto($texto,$posicion_y)
    {
        $this->texto(NORM5,null,'IVA 16%:', ANCHO + MARGENX - 41, $posicion_y + 22, 20, 7,'R');
        $this->texto(NORM5,null,$texto, ANCHO + MARGENX - 21, $posicion_y + 22, 20, 7,'R');
    }

    public function print_factura_total($texto,$posicion_y)
    {
        $this->texto(NORM5,null,'Total:', ANCHO + MARGENX - 41, $posicion_y + 30, 20, 7,'R');        
        $this->texto(NORM5,null,$texto, ANCHO + MARGENX - 21, $posicion_y + 30, 20, 7,'R');
    }

    public function print_factura_total_letra($texto,$posicion_y)
    {
        $this->texto(BOLD5, null, 'Total con letra:', MARGENX, $posicion_y + 15, ANCHO - 45, 5);
        $this->multitexto(NORM5, null, $texto, MARGENX, $posicion_y + 20, ANCHO - 45, 6);
    }

    public function print_factura_mf_pago($texto,$posicion_y)
    {
        $this->texto(NORM5, null,$texto, MARGENX, $posicion_y + 28, ANCHO - 45, 5);
    }

    public function print_factura_certificado($a,$b,$c,$posicion_y)
    {
        $this->texto(BOLD5, null, 'Certificado del SAT:', MARGENX, $posicion_y +33, 24, 5);
        $this->texto(NORM5, null, $a, MARGENX + 24, $posicion_y +33, 29, 5);
        $this->texto(BOLD5, null, 'Fecha del Timbrado:', MARGENX +53, $posicion_y +33, 25, 5);
        $this->texto(NORM5, null, $b, MARGENX + 78, $posicion_y +33, 25, 5);
        $this->texto(BOLD5, null, 'RfcProvCertif:', MARGENX + 103, $posicion_y +33, 17, 5);
        $this->texto(NORM5, null, $c, MARGENX + 120, $posicion_y +33, 26, 5);
    }

    public function print_factura_sello_cfdi($texto,$posicion_y){
        $this->texto(BOLD5, null, 'Sello Digital del CFDI:', MARGENX, $posicion_y +38, ANCHO -1, 5);
        $this->multitexto(NORM5, null, $texto, MARGENX, $posicion_y +44, ANCHO - 1, 5);
    }

    public function print_factura_sello_sat($texto,$posicion_y)
    {
        $this->texto(
            BOLD5, null, 'Sello Digital del SAT:', MARGENX + 45, $posicion_y + 57, ANCHO - 47, 5);
        $this->multitexto(NORM5, null, $texto, MARGENX + 45, $posicion_y + 62, ANCHO - 47, 5);
    }

    public function print_factura_cadena_origen($texto,$posicion_y)
    {
        $label = 'Cadena Original del complemento de certificacion digital del SAT:';
        $this->texto(BOLD5, null, $label, MARGENX + 45, $posicion_y + 77, ANCHO - 47, 5);
        $this->multitexto(NORM5, null, $texto, MARGENX + 45, $posicion_y + 82, ANCHO - 47, 5);
    }

    public function print_factura_qrcode($y) 
    {
        $estilo = array(
            'border' => false,
            'padding' => 0,
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false,
        );

        $texto = 'https://verificacfdi.facturaelectronica.sat.gob.mx/default.aspx';
        $texto .= '&id='.$this->tfd['UUID'];
        $texto .= '&re=' . $this->emisor['Rfc'];
        $texto .= '&rr=' . $this->receptor['Rfc'];
        $texto .= '&tt=' . $this->documento['Total'];
        $texto .= '&fe=' . substr($this->tfd['SelloCFD'],-8);
        $this->write2DBarcode($texto, 'QRCODE,M', MARGENX + 3, $y + 60, 38, 38, $estilo, 'N');
    }

    private function print_plantilla_conceptos()
    {
        $this->Rect(MARGENX, 65, ANCHO, 7, 'F', null, BLUEG1); 

        $this->multitexto(NORM4,null,'Clave ProdServ', MARGENX+2, 65, 13, 7,'C');
        $this->multitexto(NORM4,null,'Clave Unidad', MARGENX+15, 65, 12, 7,'C');
        $this->texto(NORM4,null,'Unidad', MARGENX+27, 65, 12, 7,'C'); 
        $this->texto(NORM4,null,'Cantidad', MARGENX+39, 65, 12, 7,'L');        
        $this->texto(NORM4,null,'Descripcion', MARGENX+51, 65, 110, 7);
        $this->texto(NORM4,null,'Precio', MARGENX+161, 65, 16, 7,'C');
        $this->texto(NORM4,null,'Importe', MARGENX+177, 65, 19, 7,'C');
    }

    public function print_plantilla_pie($posicion_y)
    {
        $estilo = array('width' => 0.1, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => BLUEG6);
        $this->Rect(MARGENX, $posicion_y + 39, ANCHO, 18, 'F', null, BLUEG1);
        $this->Line(MARGENX, $posicion_y + 13, ANCHO + MARGENX, $posicion_y + 13,$estilo);
        $this->Rect(MARGENX + ANCHO - 40, $posicion_y + 15, 40, 7, 'F', null, BLUEG1);
        $this->Rect(MARGENX + ANCHO - 40, $posicion_y + 23, 40, 7, 'F', null, BLUEG1);
        $this->Rect(MARGENX + ANCHO - 40, $posicion_y + 31, 40, 7, 'F', null, BLUEG1);
    }

    public function texto($font, $color = null, $text, $x = 0, $y, $width = 0, $height = 0, $align = 'L')
    {
        $border = 0;
        if (!is_null($color)) {
            $dr = (int) $color[0];
            $dg = (int) $color[1];
            $db = (int) $color[2];
        } else {
            $dr = 0;
            $dg = 0;
            $db = 0;
        }

        $this->SetXY($x, $y);
        $this->SetTextColor($dr, $dg, $db);
        $this->SetFont(PDF_FONT_NAME_MAIN, $font[1], $font[2]);
        $this->Cell($width, $height, $text, $border, false, $align);
    }

    public function multitexto($font, $color = null, $text, $x, $y, $w, $h, $align = 'L',$html = false) 
    {
        //$w, $h, $txt, $border=0, $align='J', $fill=false, $ln=1, $x='', $y='', $reseth=true, $stretch=0, $ishtml=false, $autopadding=true, $maxh=0, $valign='T', $fitcell=false)
        $border = 0;

        if (!is_null($color)) {
            $dr = (int) $color[0];
            $dg = (int) $color[1];
            $db = (int) $color[2];
        } else {
            $dr = 0;
            $dg = 0;
            $db = 0;
        }

        $this->SetXY($x, $y);
        $this->SetTextColor($dr, $dg, $db);
        $this->SetFont(PDF_FONT_NAME_MAIN, $font[1], $font[2]);
        $this->MultiCell($w, $h, $text, $border, $align, 0, 0, $x, $y,true,0,$html);
    }
}