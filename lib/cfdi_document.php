<?php
/**
 * Description of ______________
 *
 * @author Juan Jose Prieto Dzul
 * @copyright 2016, XXXX. All Rights Reserved.
 */
use \CfdiUtils\CfdiCreator33; 
use \CfdiUtils\XmlResolver\XmlResolver;

class cfdi_document
{
    private $certificado;
    private $sello;

    private $documento;
    private $receptor;
    private $emisor;
    private $factura; 
    private $tipodocumento;
    private $cfdirelacionado;

    private $creator;

    public function __construct($factura,$data,$localxslt)
    {
        $this->factura = $factura;
        switch ($data['tipo']) {
            case 'egreso':
                $tipodocumento = 'E';
                $usodocumento = 'G02';
                break;            
            default:
                $tipodocumento = 'I';
                $usodocumento = $data['uso'];
                break;
        }
        $this->tipodocumento = $data['tipo'];
        $this->cfdirelacionado = isset($data['uuid']) ? $data['uuid'] : false ;

        if ($factura->pagada) {
            $metodopago = 'PUE';
            $formapago = ($tipodocumento==='E') ? '01' : $factura->codpago;
        } else {
            $metodopago = 'PPD';
            $formapago = '99';
        }   

        $documento = [
            "Serie" => $factura->codserie,
            'Folio' => $factura->codigo,
            'Fecha' => $this->fecha_hora($factura->fecha, $factura->hora),
            'FormaPago' => $formapago,
            'Moneda' => $factura->coddivisa,
            'TipoCambio' => '1',
            'TipoDeComprobante' => $tipodocumento,
            'MetodoPago' => $metodopago,
            'LugarExpedicion' => $data['expedicion'],
            'Descuento' => 0.00,
        ];

        $this->emisor = [
            'RegimenFiscal' => $data['regimen'],  
        ];

        $this->receptor = [
            'Rfc' => $factura->cifnif,
            'UsoCFDI' => $usodocumento,
        ];

        if ($data['tipo'] != 'global') {
            $this->receptor['Nombre'] = $factura->nombrecliente;
        }

        //$pathtofile = dirname(__DIR__, 3) . '/' . $localxslt . '/';
        $resolver = new \CfdiUtils\XmlResolver\XmlResolver($localxslt);      

        $this->creator = new CfdiCreator33($documento);
        $this->creator->setXmlResolver($resolver);

        $this->documento = $this->creator->comprobante();
    }

    public function setConceptos()
    {
        switch ($this->tipodocumento) {
            case 'egreso':
                $this->documento->addConcepto([
                    'ClaveProdServ' => '84111506',
                    'NoIdentificacion' => '00',
                    'Cantidad' => 1,
                    'ClaveUnidad' => 'ACT',
                    'Unidad' => 'Actividad',
                    'Descripcion' => 'Devolucion de mercancias',
                    'ValorUnitario' => $this->redondeo_precio($this->factura->neto),
                    'Importe' => $this->redondeo_precio($this->factura->neto),
                    'Descuento' => 0,
                ])->addTraslado([
                    'Impuesto' => '002',
                    'Base' => $this->redondeo_precio($this->factura->neto),
                    'TipoFactor' => 'Tasa',
                    'TasaOCuota' => $this->redondeo_precio(0.16, 6),
                    'Importe' => $this->redondeo_precio($this->factura->totaliva),
                ]);
                break;

            case 'global':
                $albaran0 = new albaran_cliente();
                #$a = 0;
                #$b = 0;
                foreach ($this->factura->get_albaranes() as $albaran) {
                    $albaran = $albaran0->get($albaran);
                    $totales = $albaran->_totales();

                    $this->documento->addConcepto([
                        'ClaveProdServ' => '01010101',
                        'NoIdentificacion' => $albaran->codigo,
                        'Cantidad' => 1,
                        'ClaveUnidad' => 'ACT',
                        'Descripcion' => 'Venta',
                        'ValorUnitario' => $this->redondeo_precio($totales['total'],6),
                        'Importe' => $this->redondeo_precio($totales['total'],6),
                        'Descuento' => $this->redondeo_precio(0,6),
                    ])->addTraslado([
                        'Impuesto' => '002',
                        'Base' => $this->redondeo_precio($albaran->neto),
                        'TipoFactor' => 'Tasa',
                        'TasaOCuota' => $this->redondeo_precio(0.16, 6),
                        'Importe' => $this->redondeo_precio($totales['totaliva'],6),
                    ]);
                    #$a = += $totales['total'];
                    #$b = += $totales['totaliva'];
                }
                #$this->new_message('TOTAL : ' . $this->format_precio($a));
                #$this->new_message('TOTAL IVA : ' . $this->format_precio($b));
                # code...
                break;

            case 'pago':
                # code...
                break;
            
            default:
                $articulo0 = new articulo();

                foreach ($this->factura->get_lineas() as $linea) {
                    $articulo = $articulo0->get($linea->referencia);

                    $this->documento->addConcepto([
                        'ClaveProdServ' => $articulo->clavesat(),
                        'NoIdentificacion' => $linea->referencia,
                        'Cantidad' => sqrt($linea->cantidad ** 2),
                        'ClaveUnidad' => $articulo->claveum(),
                        'Unidad' => $articulo->nombreum(),
                        'Descripcion' => $linea->descripcion,
                        'ValorUnitario' => $this->redondeo_precio($linea->pvpunitario,6),
                        'Importe' => $this->redondeo_precio($linea->pvpsindto,6),
                        'Descuento' => $this->redondeo_precio($linea->dto_total(),6),
                    ])->addTraslado([
                        'Impuesto' => $linea->codimpuesto,
                        'Base' => $this->redondeo_precio($linea->pvptotal,6),
                        'TipoFactor' => 'Tasa',
                        'TasaOCuota' =>  $this->redondeo_precio(0.16, 6),
                        'Importe' => $this->redondeo_precio($linea->iva_total(), 6),
                    ]);
                }
                break;
        }       

        $this->creator->addSumasConceptos(null, 2);
    }

    public function setCertificado($certificado)
    {
        $this->certificado = new \CfdiUtils\Certificado\Certificado($certificado);
    }

    public function setLlave($llave,$password)
    {
        $this->llave = $llave;
        //$pemFile = "file://".dirname(dirname(__DIR__)) . getenv("PEM_PATH");
        //$creator->addSello($pemFile, $password);
    }

    public function setRelacionados()
    {
        if ($this->cfdirelacionado) {
            $this->documento->addCfdiRelacionado([
                'UUID' => $this->cfdirelacionado,
            ]);
            $this->documento->getCfdiRelacionados()->addAttributes([
                'TipoRelacion' => '01'
            ]);
        }
    }

    public function generarXml()
    {
        $this->setRelacionados();
        $this->documento->addEmisor($this->emisor);
        $this->documento->addReceptor($this->receptor);
        $this->creator->putCertificado($this->certificado, true);

        $this->setConceptos();
        $this->creator->addSello($this->llave);

        //$path = dirname(__DIR__, 3) . '/plugins/cfdi/sat/xml.xml';
        //$this->creator->saveXml($path);
        return $this->creator->asXml();
    }

    private function fecha_hora($f, $h)
    {
        return date("Y-m-d", strtotime($f)) . 'T' . date("H:i:s", strtotime($h));
    }

    private function redondeo_precio($cantidad = 0, $decimales = 2)
    {
        $cantidad = sqrt($cantidad ** 2);
        return number_format($cantidad, $decimales, '.', '');
    }
}
