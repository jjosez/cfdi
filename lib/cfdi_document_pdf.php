<?php
/**
 * Description of ______________
 *
 * @author Juan Jose Prieto Dzul
 * @copyright 2016, XXXX. All Rights Reserved.
 */
require_once 'cfdi_tcpdf.php';
require_once 'cfdi_document_reader.php';

define('NORM1', array('Arial', '', 20));
define('BOLD1', array('Arial', 'B', 20));
define('NORM2', array('Arial', '', 15));
define('BOLD2', array('Arial', 'B', 15));
define('NORM3', array('Arial', '', 12));
define('BOLD3', array('Arial', 'B', 12));
define('NORM4', array('Arial', '',8));
define('BOLD4', array('Arial', 'B', 8));
define('NORM5', array('Arial', '',7));
define('BOLD5', array('Arial', 'B', 7));
define('NORM6', array('Arial', '',6));
define('BOLD6', array('Arial', 'B', 6));

define('CAZUL1', array(3, 155, 229));
define('CGRIS3', array(144, 164, 174));
define('BLUEG6', array(84, 110, 122));
define('BLUEG1', array(207, 216, 220));

class cfdi_document_pdf {
    private $pdf;
    
    private $emisor;
    private $receptor;
    private $documento;
    private $tfd;
    private $relacionados;
    private $impuestos;
    private $tImpuestosTrasladados;
    private $tImpuestosRetenidos;

    private $limitex;
    private $limitey;
    private $limiteu;

    private $logo = 'plugins/cfdi/view/extras/logo/logo2.png';

    public function __construct($xml) {
        $cfdi = new cfdi_document_reader($xml);

        $this->emisor = $cfdi->Emisor();
        $this->receptor = $cfdi->Receptor();;
        $this->documento = $cfdi->Documento();
        $this->conceptos = $cfdi->Conceptos();
        $this->tfd = $cfdi->Tfd();
        $this->relacionados = $cfdi->Relacionados();
        $this->impuestos = $cfdi->Impuestos();
        $this->tImpuestosTrasladados = $cfdi->TotalImpuestosTrasladados();
        $this->tImpuestosRetenidos = $cfdi->TotalImpuestosRetenidos();

        $headerData = array(
            'documento' => $this->documento,
            'emisor' => $this->emisor,
            'receptor' => $this->receptor,
            'relacionados' => $this->relacionados,
            'tfd' => $this->tfd
        );

        $this->pdf = new CFDI_TCPDF($headerData,PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT);

        $this->pdf->SetAuthor('Facturascripts / CFDI-Mexico');
        $this->pdf->SetHeaderData($this->logo, 50);
        $this->pdf->AddPage();
    }

    public function generar_pdf($nombre='cfdi', $as = 'I') {
        $this->preparar_documento();
        $nombre .= '.pdf';
        $this->pdf->Output($nombre,$as);
    }

    public function preparar_documento() {  
        $this->print_conceptos();         
    }

    public function print_conceptos() 
    {
        $posicion_y = 72;
        $numero_pagina = 0;
        $numero_linea = 0;
        $limite_linea = 35;

        foreach ($this->conceptos as $concepto) {
            $this->pdf->print_concepto_cantidad( $concepto['Cantidad'],$posicion_y);
            
            $this->pdf->print_concepto_um($concepto['ClaveUnidad'],$posicion_y);

            $unidad = $concepto['Unidad'] ? : 'No aplica';
            $this->pdf->print_concepto_unidad($unidad,$posicion_y);

            $this->pdf->print_concepto_clave($concepto['ClaveProdServ'],$posicion_y);

            $descripcion = $concepto['NoIdentificacion'] . ' - ' . $concepto['Descripcion'];
            $this->pdf->print_concepto_descripcion($descripcion,$posicion_y);

            $this->pdf->print_concepto_precio($concepto['ValorUnitario'],$posicion_y);
            $this->pdf->print_concepto_importe($concepto['Importe'],$posicion_y);

            $posicion_y +=5;
            if ($numero_linea==$limite_linea) {
                $this->pdf->AddPage();
                $posicion_y = 72;
                $numero_linea = 0;
                $numero_pagina++;
            } else {
                $numero_linea++;
            }    
        }

        if ($posicion_y > 165) {
            $this->pdf->AddPage();
        }

        $posicion_y = 160 ;
        $this->print_documento_pie($posicion_y);
        $this->print_documento_totales($posicion_y);
    }

    public function print_documento_totales($y) {        
        $texto = fs_format_precio($this->documento['SubTotal']);        
        $this->pdf->print_factura_subtotal($texto,$y);

        $texto = fs_format_precio($this->tImpuestosTrasladados);
        $this->pdf->print_factura_impuesto($texto,$y);

        $texto = fs_format_precio($this->documento['Total']);
        $this->pdf->print_factura_total($texto,$y);
    }

    public function print_documento_pie($y)
    {
        $this->pdf->print_plantilla_pie($y);

        $texto = fs_numero_a_letra(fs_format_precio($this->documento['Total']),'MXN');
        $this->pdf->print_factura_total_letra($texto,$y);

        $metodop = get_catalogo_cfdi('MetodoPago', $this->documento['MetodoPago']);
        $texto = 'Metodo de pago: '. $metodop->id . ' ' . $metodop->descripcion;
        $formap = get_catalogo_cfdi('FormaPago', $this->documento['FormaPago']);
        $texto .= ' | Forma de pago: '. $formap->id . ' ' . $formap->descripcion;
        $this->pdf->print_factura_mf_pago($texto,$y);

        $this->pdf->print_factura_certificado($this->tfd['NoCertificadoSAT'],
            $this->tfd['FechaTimbrado'],$this->tfd['RfcProvCertif'],$y);

        $this->pdf->print_factura_sello_cfdi($this->tfd['SelloCFD'],$y);

        $this->pdf->print_factura_sello_sat($this->tfd['SelloSAT'],$y);

        $texto = '||' . $this->tfd['Version'] . '|' . $this->tfd['UUID'] . '|' . $this->tfd['SelloCFD']
        . '|' . $this->tfd['NoCertificadoSAT'] . '||';
        $this->pdf->print_factura_cadena_origen($texto,$y);
        $this->pdf->print_factura_qrcode($y);
    }
}